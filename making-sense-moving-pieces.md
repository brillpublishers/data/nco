# Making Sense Moving Pieces

### Repos and Branches

1. **Client**: https://gitlab.com/brillpublishers/arkyves-frontend/tree/feature/making-sense-deploy
1. **Server**: https://gitlab.com/brillpublishers/code/kosmeo/tree/feature/add-docker
1. **Data**: https://gitlab.com/brillpublishers/making-sense

## Data Pipeline

### Stage 1

This is the source of truth:

1. https://gitlab.com/brillpublishers/making-sense/tree/master/data_model/data/metamorfoze_metadata/csv
1. https://gitlab.com/brillpublishers/making-sense/tree/master/data_model/data/monk_NNM_20190625/input

### Stage 2

Data needs to be converted into DMP file format, which is very similar to what has already been done:

1. https://gitlab.com/brillpublishers/making-sense/blob/master/data_model/csv_to_txt_metamorfoze_metadata.py
1. https://gitlab.com/brillpublishers/making-sense/blob/master/data_model/parser_monk_nnm.py

[This ticket](https://trello.com/c/eaBx6OTL/45-update-scripts-for-converting-metamorfozemetadata-and-monk-output-data) needs to be worked though to take the DMP files on the H2 server that Etienne manually edited to create the natural keys, add the data types, and include parent fields.

Question: where should the DMP files be stored?

### Stage 3

Load the data into ElasticSearch using the Django load management command found [here](https://gitlab.com/brillpublishers/code/kosmeo/blob/feature/add-docker/kosmeo/management/commands/load.py).
