# Making Sense of Illustrated Handwritten Archives

This repository is concerned with a **subproject** of a larger project. The aim of this subproject is to create a "site" for NCO.

NCO is a digital primary source collection.

This is a collection in the field of natural history. It consists of the objecs, drawings, diaries, correspondence, and notes collected and created by the [Natuurkundige Commissie](https://nl.wikipedia.org/wiki/Natuurkundige_Commissie_voor_Nederlands-Indië).

The Natuurkundige Commissie voor Nederlandsch-Indië (1820-1850) was a body of scientists and draftsmen sent to the Dutch East Indies in the early nineteenth century to collect minerals, plants, and animals.

The collection is preserved in [Naturalis](https://www.naturalis.nl/en). It was digitized in 2007 in the context of [Metamorfoze](https://www.metamorfoze.nl/projecten-alles), a Dutch national digital preservation program. 

In Metamorfoze, only images were created, with metadata. It is not possible to textually search the field books, or to semantically search the drawings and objects.

The first aim of the project [Making Sense of Illustrated Handwritten Archives](https://sites.google.com/naturalis.nl/makingsenseproject) is to create an "index" of relevant terms of NCO through the use of HWR. [HWR](https://en.wikipedia.org/wiki/Handwriting_recognition) is handwriting recognition (a mode of image recognition) through machine learning. The software applied is that of the [MONK](http://www.ai.rug.nl/~lambert/Monk-collections-nl.html) project.

A second aim is to semantically enrich the drawings and objects.

In this way, the collection will be more discoverable than before, and its valuable content opened up to a larger group of biologists, historians, and anyone else in interested in biodiversity and the history of science.

The NCO collection, enriched through handwriting recognition and semantic annotation, will be published by Brill on a site that is yet to be created.