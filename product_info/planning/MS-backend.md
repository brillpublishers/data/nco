# MS Backend

## Arkyves Infrastructure

* presently a monolith, but about to be teased apart
* presently a React/JS front end
* presently a Python back end
* This monolith will be come a "module" on data.brill.com
* data.brill.com is the generic platform, offering generic functionality, such as authentication and indexing
* on top of that sit specific "modules" or applications (microservices)
* This has already been done with SEG
* This is what we will do with NCO, too

## Arkyves clones

* the following publications are planned for 2019 and later
* they will be modules, too
* they will be a "clone", or copy of the Arkyves back end and front end, with changes for specific requirements (e.g., PDFs for LCI)
   * Chinese Iconography Thesaurus (CIT)
   * Lexikon der christlichen Ikonographie (LCI)
   * Annotated Books Online
   * Luchtmans Archief

## tools
* rescribe tool (= transcription)
* Lise's annotator

## The plan
The plan for the infrastructure is as follows:
* NCO as module on data.brill.com platform
* databases for images and data (metadata, MONK index terms, triples) and iiif APIs
* archiving in GitLab
* SPARQL end point
* front end Arkyves clone, adapted to fit the NCO needs

## NCO
* will be another module
* will be another Arkyves clone
* starting with copying the Arkyves front end, but with changes for specific requirements
* the back end consists of Python code
* and of a database for the images, and a database for data (metadata[^1], MONK's index terms, Lise's triples)
* using the iiif APIs (image API, presentation API)
* data will be archived in GitLAb (access for Naturalis)
* apparently, we need a SPARQL end point, so a SPARQL triple engine will be built on top Lise's triples
* already in place: iiif server (for Arkyves) and data.brill.com

There is the problem that Arkyves is in React (JS) and Eldarion prefer to work in Vue (JS). 

## An illustration
This is how we envisaged the architecture (late 2018)

![](product_info/wireframes/nco-wireframe-architecture.jpg)
