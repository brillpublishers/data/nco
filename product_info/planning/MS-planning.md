# MS and NCO Planning

## NCO Project

* NCO as a project is dependent on CIT, which is basically the teasing apart of the current Arkyves monolith. But there is no planning for CIT, so this is arisk.
* proposed realization phase is Q2-Q3
* we will need extra staff (Lianne Heslinga, and also "someone like George Hickman")
* exact number of hours required for that "someone" can be established when we have a better grasp of the requirements (this is a question to Naturalis)
* expected activities for that "someone" include front end work, back end development, and data cleaning
* budget needs to come from Brill's financial contribution to the Making Sense project
* work pressure means we can't starting looking for that "someone" before February
* The taskgroup needs to revise the requirements and the mockups (and be involved in all further steps)

## Calendar
* Task group 5 to meet in April
* demo to Naturalis, September 2019
* demo at [BiodiversityNext](https://www.icedig.eu/content/biodiversity-next-better-data-better-science-better-policies-open-digital-science-week), October 2019
* happening at lustrum Naturalis, October 2020

## Development team
* Brill: EP, LH, ES
* external: ? (we have funding for 16 hrs a week for 24 weeks)
* MS Taskgroup 5
* user panel (interviewees)
* wider MS and Naturalis stakeholders

