# MS/NCO Kick-off 10 April 2019

James Tauber (Eldarion), Michael Herman (Eldarion), Lianne Heslinga (Brill), Etienne Posthumus (Brill), Ernest Suyver (Brill)

## Architecture

We started out with the idea of an Arkyves back-end and an Arkyves front end, then tended towards an Scaife front end, and then towards a back end of Arkyves in a different form, sc. micro-services.

**So now the idea is to take the best of Arkyves, turn it into micro-services, and hook-up to Scaife.** Skeleton Scaife.

And to do stuff with GitLab Runner, Register and Docker. But not with SemanticUI (React) or Transcrypt. 

This idea might also work for **primary sources**. Brill is considering retiring [its current platform](https://primarysources.brillonline.com/). 

(Personal interjection: I was the project manager for this one; everybody is alway bitching about it, but was the best we could do within the constraints that we had, and it was such an improvement over what we had before).

In addition to the iiif APIs, we will need a **CITE/DTS API**. Which we also absolutely **must have** for SEG, Jacoby, etc.

And Scaife is no tmarried to CTs, or at least not to Nautilus, as the Homer site proves.

So there is this blurring of distinction between Scholarly Editons, NCO, Arkyves, and other stuff, such facsimiles (for which Erasmus's _Adagia_ would be a good showcase, as it is in Arkyves, with images and P4 tei xml).

Use OpenSeaDragon instead of Mirador viewer? (But Mellon and Johns Hopkins want an experiment with Scaife and Mirador).

## Data, user requirements

Data

1. Scans (= images)
2. Metadata
3. MONK data (labels with coordinates to link to scans, with the problem of the difference between Metamorfoze scans and MONK scans)
4. semantic data ("Lise's data")

Data in DB Text format,  data format = objects wih an ID and fields (which thus form tripples).

User requirements: rough idea. Ernest to make "User stories".

Do automated tests? Unite tests? Load tests? Smoke tests? Given the budget, we need to be pragmatic.

Who is going to do what when? Eldarion to start on front end = skeleton Scaife. 

