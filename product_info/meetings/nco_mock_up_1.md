# Just a mock up...

![](NCO-Arkyves-page.png)

![](NCO-SE-page.png)

So...

Left panel might contain two components:

1. Navigation. Using URNs, just like SE. Is this superfluous when we're using Metabotnik?

2. Search. It would be nice if we could fact the search on text, metadata and semantic data.

Right panel is more complicated:
* Inline with [BOCS](https://gitlab.com/brillpublishers/documentation-workflow/blob/master/bocs.md) we would like to see **people |  organizations | places | periods | subjects | authors | works**
* I am totally unsure how to feed this off the MEtamorfose metadata
* So maybe this:

3. time widget = timeline, feeding off Lise's data
4. place widget = map, , feeding off Lise's data
5. animal widget = ??????, feeding off Lise's data [variant of subjects]
6. people widget = ??????, feeding off Lise's data
7. references widget = ??????, maybe we can develop this together with SE 
8. Link thing = searches a given term in a number of databases. Or should this also be connected to Lise's data??? This could also be a popup like we have now in the FR doc.

#### Metadata from Metamorfose per ??
* Admin
* coderingen
* Datering
* kenmerken
* Opmerkingen
* Personen
* sleutelwoorden
* Taxonomische sleutelwoorden

#### Metadata from Metamorfose per page
* Digital object number
* File name
* tome
* Subject
* Content
* Page number
* Page number pencil
* Species name original
* Species name full
* Image caption
* Image reference
* Corresponding file
* Illustrator Geography
* Remarks

PS Is it a problem that Arkyves (like SEG EE) is in React, not Vue?