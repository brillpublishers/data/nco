April 4

-	Decision: include Verhandelingen, if we can link to the archive. (Verhandelingen are c. 8% of NCO total, and available, with OCR, on BHL)
-	Decision: most important fields: Persons (viaf), Places (geonames), Time/Periods (?), Species (= subjects) and must be included in the site
-	Decision: include Lise’s annotator, i.e. allow users to keep on adding semantic data
-	Decision: design architecture in such a way that later developments can be included
-	Decision: BHL and the Bioportal are the two must-haves databases to link to
-	Decision: include illustrations
-	Observation: Datapen is potentially a nice tool
-	Question: How can we facilitate objects rather than potentially historical/changing taxonomical names as basis for exploration?
-	Question: Nano publications are nice and can we do something with them?
