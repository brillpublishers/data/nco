# Progress report Taskgroup 5

## reminder: who?

Eulalia, Lise, Andreas, Ernest

## Function

* data quality control
* user panel (Eulalia representing major stakeholder)

## The project

* goal: create interface
* timeline: c. 8 months (Christmas deadline)
* technical infrastructure: generic; focus on interface

## Brill and Eldarion

* creators (https://eldarion.com/) of Scaife viewer (https://dh.brill.com/scholarlyeditions/)
* started in April
* weekly calls with Etienne and me and the main Eldarion developer; sometimes ad hoc calls; Slack
* all documentation and code on GitLab (https://gitlab.com/brillpublishers/making-sense)
* progress so far: setting up of minimum viable product (https://brill-nco.herokuapp.com/); we're now in the middle of loading the data (this includes images, MONK outout, Metamorfoze metadata and Lise's semantic data)
