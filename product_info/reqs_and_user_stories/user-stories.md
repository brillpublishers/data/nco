# MS User Stories 

### Background
* _Template_: As a \<type of user\>, I want \<some goal\> so that \<some reason\>.
* _Example 1_: As Sascha, I want to organize my work, so I can feel more in control. 
* _Example 2_: As a manager, I want to be able to understand my colleagues' progress, so I can better report our sucess and failures. 

_Story size_:
Initiative \> Epic \> Story

### explore
* As a user, I want to see all collections, so that I know what's there.
* As a user, I want to see all parts of a collection, so that I understand its size and structure.
* As a user, I want to see all subparts of the parts of a collection, so that I get a clear view of all their details.

### search
* As a user, I want to perform a full-text search over all collections, so I can find a phrase or term I'm looking for.
* As a user, I want to text search within a collection, so I can find a phrase or term I'm looking for.
* As a user, I want to be able to filter my search results - on collection and possibly parts thereof, on subjects and possibly other categories, on information type (metadata, full-text) - , so I can get more precise or relevant results.
* As a user, I want to save my searches, so that I have them ready for next time.
* As a user, I want to be able to share my searches, so that others can look at the same results.

### read
* As a user, I want to read the documents in the collection, so that I can learn what it is about.

### connect
* As a user, I want to see if a term (species) is present in other databases, so that I can find more information about it.
* As a user, I want to have a SPARQL endpoint, so that I can query semantic data in any way I want.
* As a user, I want to see all collections, so that I know what's there.

### annotate
* As a user, I want to make annotations, so that I express my insights about the material.
* As a user, I don't want to jump through hoops but I do it because I sort of understand the need for structured information.

### cite
* As a user, I want to be able to refer to anything - a roi, a word, whatever- in a subpart or part of a collection.
* As a user, I want to be able to cite the documents I am look at, so I can share that information with others.

### see collection data
* As a user, I want to see quantative information about a collection
* As a user, I want to see how much all collections, so that I know what's there.

#### types of user
I guess we need to specify what kind of user...
* end user
* brill staff
* collection holder
* librarian (customer)
