- we need to set definitive URL

- metabotnik should be bigger
- metabotnik has sides cut off
- metabotnik doesn't actually do anything: give nno info, has no link

- trigger search when selecting a filter. 

- what does the save button do?
- save button is on top of other stuff

- we need better captions for the search results

- viewer should supper previous-next better

- eliminate book pages

- filter all search results with MONK labels

- we need an "archief index", a treelike representation of the colection. 
- tally this with breadcrumb?

- sorted-by is a bit hidden
- default value of sorted-by should be object ID

- reduce values in filter by type to publication (NOT book); correspondence; field notes; illustrations; reports/diaries

- add button "back to search results"
- now we use the browser's back button. But this often does not work

- in case where a folder contains only one item: give item a type, add the folder info to the item, and eliminate the folder

- viewer + and - steps are too big

- https://labs.brill.com/makingsense/view/nco_MMNAT01_AF_NNM001001036_049_MONK/makingsense everything is weird here: url is weird, metadata are lost, container info is lost, navigation is broken
