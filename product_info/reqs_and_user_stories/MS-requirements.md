## MS Requirements

This document lists the functional requirements for the interface of _Making Sense_ (MS). This is a public-facing interface that gives access to the Brill primary source collection _Natuurkundige Commissie Online_ (NCO). 

<!-- 
## Previously in NCO...
Changed since early 2018:
* It is no longer planned to integrate MONK into the Brill backend
* The Brill back end can be fed from MONK (this has happened already), but there is no pipeline and therefore no dependency
* Therefore, the "production environment" has been removed from the plan

Happened since early 2018:
* creation of data.brill.com
* creation of iiif server
* import of all NCO images (plus sample data) into iiif db

What | number of pages
----- | -----
Scans | 25,000
Indexed | 3,500
Semantic | 300
-->

## Description of NCO
See [here](https://gitlab.com/brillpublishers/making-sense/blob/master/data_model/collection_description.md)

## Relation to MONK
MONK is the software that recognizes handwriting. Output of MONK can be uploaded to _MS_. This means the iiif server and the ElasticSearch database are refreshed. There is **no other connection**. 

## Back end
See [here](https://gitlab.com/brillpublishers/making-sense/blob/master/product_info/the_plan/NCO-plan.md)

## Functional requirements

**End users** can

1. Explore
   1. Navigation of images. Both vertically (collection-book-page levels) and horizontally (next-previous). 
   2. Explore data (metadata, MONK index terms, and triples).
2. Search
   1. Simple/full text search (and differentiate between metadata, MONK index terms, and triples).
   2. Advanced search (metadata, MONK index terms, and triples). Facetted search: search by combining data categories.
3. View/Read
   1. Zoom, pan, rotate, adjust colors and contrast.
   2. Copy, download, print, cite. Including the copying of text and ROIs.
4. Connect. Esp. linking data (e.g. species names) to external databases (incl. the Naturalis Bioportal)
5. Annotate. Semantic annotations. (At a later stage, user may be able to transcribe, using Etienne's _rescribe_ tool)
6. Cite
7. Readily see collection data, like (qualitative and quantitive) measure of MONK-edness.

**Library administrators** can
1. manage their institution's details & upload a library/institution's logo
2. manage IP ranges and library administrator username & password
3. download usage statistics, like COUNTER 5 or SUSHI
4. download MARC21 records or other metadata files (like KBART)
5. connect the institution's database or catalogue to this platform (openURL)
6. connect this platform to an aggregator’s database
7. do this via Brill.com
8. do OpenURL 1.0

## Functional requirements (collection owners)
Collection owners can
* Add DOIs
* Naturalis URIs
* Add new content (presumably in existing collections)
* Add links to Naturalis Bioportal
* COUNTER5 and Google Analytics
* Discoverability. Google?
* API for data exchange
* tap into Naturalis API (http://bioportal.naturalis.nl/api)

<!--
## Functional requirements (institutional users)

-->
**Brill** can

1. Set access management
2. See user stats

<!-- 
## Tools
How can we fulfill these requirements?

1.1 [Metabotnik](https://metabotnik.com/) in combination with the [Mirador viewer](http://projectmirador.org)

1.2 Facets (left panel), data visualizations (maps, timelines), galleries?, lists? We need to think, too, of the difference in exploration between collection data and objects data. Also, how to deal with bibliographical data? E.g., the publications by the MS team?

2.1 Think Google. Think Delpher. Make this prominently available on all pages (e.g. “search inside” on page level). 

2.2. See 1.2. We are still looking for a good way to explore the triples (cf. [Timecapsule](http://timecapsule.science.uu.nl/timecapsule/#/login)). We also need to establish which metadata standards to support (DC, MARC, EAD?).

3. See 1.1. We need to highlight searched terms. And there needs to be interaction between the viewer and the browse/search (e.g. “Scientific names on this page”). More complex interactions with the objects (visual or textual analysis, e.g.) must be done outside the NCO environment.

4. Popup? Cf. Brill Text Tool. 

5. Lise's annotator

6. Distinguish between collections metadata and object  metadata. Support Naturalis DOIs and URIs.

7. Cf. de _Vele Handen_ widget
-->

<!-- 
## Access/User management
* Individual users can save and share queries?
* Create profiles over and above the session? NO

## Potential suppliers/developers
1. Developer of interface of Vele Handen?
2. Developer of https://notarieel.archief.amsterdam/, sc. https://hyper.id/
-->

<!--
### Other points
1. Andreas says we need to “keep” the “Metamorfoze codes”, i.e the file names of the original scans. (NCO was digitized within the Metamorfoze program). How?
2. MONK and Lise are describing different digital objects, to wit scans of verso and recto separated and scans of verso and recto combined respectively. How to fix this?
-->





