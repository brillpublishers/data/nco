## Metadata formats

We need to think about metadata and formats, for our purposes (file managament, drive functionalities) and to offer our customers (discovery, exchange formats). (And to require of collection holders that wish to publish on this PSO platform).

for example

*METS-MODS-ALTO*

We need to find out more about the [iiif presentation api](https://iiif.io/api/presentation/2.1/#introduction).

### Overview

* [METS](http://www.loc.gov/standards/mets/): describes structure of digital object (e.g. a book) and connects different repesentations of the same object
* [Dublin Core](http://dublincore.org/): descriptive metadata standard
* [MODS](http://www.loc.gov/standards/mods/): descriptive metadata standard (human readable fields)
* [Marc21](https://www.loc.gov/marc/bibliographic/): descriptive metadata standard (numerical fields)
* [ALTO](https://www.loc.gov/standards/alto/): describes lay-out of object
* [NISO/MIX](http://www.loc.gov/standards/mix/): technical metadata (images)
* [MPEG-21/DIDL](http://xml.coverpages.org/mpeg21-didl.html): not open, industry support
* [OAI-ORE](https://www.openarchives.org/ore/): supports multiple collections/repos
* [Darwin Core](https://dwc.tdwg.org/): Lise uses this...
