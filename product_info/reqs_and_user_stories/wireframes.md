## Wireframes

1. Product home page
  * URL: `https://dh.brill.com/pso/nco` (Dev is now `https://labs.brill.com/makingsense/`).
  * The sitehope mage _is_ the product home page. We will worry about a home page of ocllections, and about cross-product search later (when we add more products).
  * Example: `http://dev.arkyves.org/r/` but I like this much better `https://www.geheugenvannederland.nl/nl/geheugen/pages/collectie/Collectie+Natuurkundige+Commissie+voor+Nederlands-Indi%C3%AB`
  * Search bar on top, three "boxes" below (for print, illustrations and handwritten = subcollections). Blurb below. (The blurb is text, an explanation of the collection. This is hand-edited .md)
  * Where to show collection metadata?
  * Optional: left menu folds in/out to offer exploration (search and browse) options

![](product_info/wireframes/nco-wireframe-home.jpg)

2. Search
  * URL: `dh.brill.com/makingsense/search` (is now: `/r/section/all`. Cf SE: `https://dh.brill.com/scholarlyeditions/search/?q=ship&kind=form`)
  * This is a page that combines search and search results.
  * A dedicated page, with all facets and tools. (URL, see above). This is, at the same time, the search results page.
  * Example: http://dev.arkyves.org/r/section/him_NCO.
  * Facets go left. Should this be a fold in/out? And on every page?
  * Facets are the properties of the MS/NCO objects (including TYPE).
  * Data visualization / search tools go right. Time line and map. See [here](https://labs.brill.com/flg/) for an example. And [this](https://www.arkyves.org/view/geocontextutrecht) is an even better example. Or [here](https://chinesedeathscape.supdigital.org/).
  * Search resuls are objects, (Never collections). Uniform tile size.

![](product_info/wireframes/nco-wireframe-search.jpg)

3. Explore (browse, navigate)
  * follow cite urn. The structure is: `collection > object`. A collection can have a collection as child; an object can have an object as child. Thus, a chain of any length can be built.
  * Role for metabotniks? (Are metabotniks created by hand?)
  * Nice tiles [here](https://www.vangoghmuseum.nl/nl/belicht/brieven)

4. Read (= the viewer)
  * [OpenSeadragon](https://openseadragon.github.io) or [Mirador](https://projectmirador.org)?
  * Ribbon/gallery at bottom of page. Arrows to the side for horizontal navigation.
  * Where to display metadata?
  * Example: `https://www.geheugenvannederland.nl/nl/geheugen/view/Rhinoceros%20sumatranus%20Natuurkundige%20commissie%20Indi?facets%5BcollectionStringNL%5D%5B%5D=Collectie+Natuurkundige+Commissie+voor+Nederlands-Indi%C3%AB&coll=ngvn&maxperpage=4&page=10&identifier=NAT01%3ANNM001000864-299`

![](product_info/wireframes/nco-wireframe-read.jpg)

5. Tools
  * Menu folds in from the right
  * Menu offers information and tools
  * Click on tools to enlarge or go to search/explore page

![](product_info/wireframes/nco-wireframe-tools.jpg)

5a. Annotate
  * enlarge tool (from right menu) to transcribe
  * this tool overlays the text
  * select a word or ROI to annotate (and right-click?)
  * annotation tool can sit anywhere on page

![](product_info/wireframes/nco-wireframe-annotate.jpg)

5b. Transcribe
  * enlarge tool (from right menu) to transcribe
  * this tool overlays the text
  * select a word or ROI to annotate (and right-click?)
  * annotation tool can sit anywhere on page

5c. Connect
  * Select a word or ROI (or have site highlight it for you) that has connections
  * This brings up a popup with information and options for futher exploration
  * This is basically another exploration tool (i.e. can be used for internal exploration as well)

![](product_info/wireframes/nco-wireframe-connect.jpg)

<!-- 
Not sketched:
* [Metabotnik](https://metabotnik.com/)-type vertical exploration. What are the consequences for the URLs?
* Andreas is enamored of the horizontal exploration on this [site](https://notarieel.archief.amsterdam/). Maybe when we have more collections?
--> 

### Mock ups
* not available yet...

### Protoytypes
* not available yet...