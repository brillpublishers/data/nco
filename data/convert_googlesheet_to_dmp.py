"""NCO Data sheets notes on conversion

The .xlsx files we had were not imported 1-1 into the Google sheets, there are small differences whichs that we can not just do a "Save-As" amnd re-use the data.
So we will need to have a script.
Also, for some reason, the "ID" column is empty, and this needs to be artifically mapped again.

We export the Googlesheets as Excel, and then run this script to get new .dmp files.

For:
https://docs.google.com/spreadsheets/d/1uceqDwBUFDlm3ddLHagA-I17agGF22DllcvysnIm5_s/edit#gid=1712673218
The "handwritten" objects.

Of interest to use are the fields:
Place_verbatim  Place_interpreted   Place_current   Date

NOTE that some columns are hidden in the Google Doc!
We can use the Nauralis_ID column for the ID


For:
https://docs.google.com/spreadsheets/d/1xORNlwoECBqxjlJj5pYe3PAs6t8Cd2FS/edit?dls=true#gid=1984356958
The "drawing" objects

Of interest to us are the fields:
Place_verbatim  Place_interpreted   Place_current   Species_verbatim    Species_interpreted Species_current Date

We need to scane through those and update the .dmp files we have.
Split on semi-colon in the field (they are nog split by newline in the spreadsheet) and make the values unique. There are several duplicates for example:
februar; februar; februar; Oct.;  April





"""
import textbase
import xlrd
from pprint import pprint

def get_fieldnames_index(sheet):
    # Get the fieldnames from row 0
    fieldnames = {}
    for idx, cell in enumerate(sheet.row(0)):
        if not cell.value:
            continue
        fieldnames[cell.value.upper()] = idx
    return fieldnames

def do_handwritten():
    data = {}
    handwritten_googlesheet = xlrd.open_workbook("handwritten_googlesheet.xlsx").sheets()[0]
    fieldnames = get_fieldnames_index(handwritten_googlesheet)
    for row_i in range(1, handwritten_googlesheet.nrows):
        row = handwritten_googlesheet.row(row_i)
        # Note, the IDs are prefixed
        ID = "nco_%s" % row[fieldnames.get("NATURALIS_ID")].value

        tmp = {}
        for fieldname in ("PLACE_VERBATIM", "PLACE_INTERPRETED", "PLACE_CURRENT", "DATE"):
            val = row[fieldnames.get(fieldname, -1)].value
            if val:
                tmp[fieldname] = [x.strip() for x in set(str(val).split(";")) if x.strip()]
        if tmp:
            data[ID] = tmp
    handwritten = textbase.parse("handwritten.xlsx.dmp")
    for obj in handwritten:
        if obj["ID"][0] in data:
           obj.update(data[obj["ID"][0]])
    handwritten.dump("handwritten.dmp")

def do_drawing():
    data = {}
    drawing_googlesheet = xlrd.open_workbook("drawing_googlesheet.xlsx").sheets()[0]
    fieldnames = get_fieldnames_index(drawing_googlesheet)
    for row_i in range(1, drawing_googlesheet.nrows):
        row = drawing_googlesheet.row(row_i)
        # Note, the IDs are prefixed
        ID = "nco_%s" % row[fieldnames.get("URN")].value.replace(".", "_")

        tmp = {}
        for fieldname in ("PLACE_VERBATIM", "PLACE_INTERPRETED",  "PLACE_CURRENT", "SPECIES_VERBATIM", "SPECIES_INTERPRETED", "SPECIES_CURRENT", "DATE"):
            val = row[fieldnames.get(fieldname, -1)].value
            if val:
                tmp[fieldname] = [x.strip() for x in set(str(val).split(";")) if x.strip()]
        if tmp:
            data[ID] = tmp
    drawing = textbase.parse("image.xlsx.dmp")
    for obj in drawing:
        if obj["ID"][0] in data:
           obj.update(data[obj["ID"][0]])
    drawing.dump("image.dmp")



if __name__ == "__main__":
    do_handwritten()
    do_drawing()
