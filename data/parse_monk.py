import os
import sys
import textbase


def read_monk_file(filepath):
    """The data in the MONK downloads look like:
MMNAT01_AF_NNM001001033_005 <x> 3781 </x> <y> 214 </y> <w> 68 </w> <h> 59 </h> <txt>den</txt>
It is not really a CSV, nor is it a XML, or anything else really strcutured.
So for now we will do a very simple straightforward parsing removing the pseudotags and spaces.
If the MONK output ever changes we can revisit this conversion and make it more robust or structured.

When run, this script outputs DMP files containing the annotation.

"""

    data = {}
    for line in open(filepath, encoding="iso8859-1"):
        fields = line.strip().split(" ")

        # There seems to be a mismatch in image filenames
        # The .jpg files we have are _AF_ but the MOnk data has _PM_
        ID = fields[0].replace("_PM_", "_AF_")
        rest = " ".join(fields[1:])
        bare = (
            rest.replace("<x> ", "")
            .replace(" </x> <y> ", " ")
            .replace("</y> <w> ", "")
            .replace(" </w> <h> ", " ")
            .replace(" </h> <txt>", " ")
            .replace("</txt>", "")
        )
        data.setdefault(ID, []).append(bare)
    return data


def obj_dmp(obj):
    buf = []
    for k, v in obj.items():
        vv = "\n; ".join(v)
        buf.append(f"{k} {vv}\n")
    buf.append("$\n")
    return "".join(buf)


def dump(handwritten, data):
    parents = {}
    for obj in handwritten:
        for i in obj.get("URL.IMAGE", []):
            ii = i.replace(".jpg", "")
            if ii in data:
                parents[ii] = obj["ID"]
                obj["LINK.ANNOT"] = [f"nco_{ii}_MONK"]
    handwritten.dump("handwritten.xlsx.dmp")

    with open("monk.dmp", "w") as F:
        for key, val in data.items():
            tmp = {
                "ID": [f"nco_{key}_MONK"],
                "PARENT": parents[key],
                "ANNOT.WORD": val,
                "TYPE": ["annotation"],
            }
            F.write(obj_dmp(tmp))


if __name__ == "__main__":
    # Parse the handwritten objects data file so we can match imagefilenames to object IDs
    handwritten = textbase.parse("handwritten.xlsx.dmp")

    all = {}
    for filename in sys.argv[1:]:
        if not filename.lower().endswith("+txt"):
            continue
        data = read_monk_file(filename)
        all.update(data)
    dump(handwritten, all)
