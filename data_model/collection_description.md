## Description of NCO

* NCO is a primary source collection
* NCO is an abbreviation for _Natuurkundige Commissie Online_. This is the name of the Brill product.
* The collection consists of objects
* There are three main types of objects:
  1. Proceedings (NL: Verhandelingen)
  2. Drawings (NL: Tekeningen)
  3. Handwritten (NL: no umbrella term is used)
* The historical background is that the Natuurkundige Commisie first produced field notes, i.e handwritten documents, and that from them, at a later stage, later printed books ("Verhandelingen") were produced. 
* I am unsure where the drawings fit in. There are drawings in the proceedings. There are drawings in the field notes. There are also separate drawings.
* We're looking here mainly at files and folders

### Object Type 1: Proceedings

* In the 19th century,  11 _Proceedings_ were published in three volumes. _Proceedings_ are academic reports. These volumes are all over the internet, for example on <archive.org>, with full-text and beautiful drawings.
* The title of these Proceedings is (in Dutch) as follows _Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen_
* The three published volumes cover, respectively:
  - ethnography (NL: Land- en Volkenkunde)
  - zoology (NL: Zoölogie)
  - botany (NL: Botanie)
* There are 11 objects of the type `Proceeding`
* Their identifiers range from `NNM001000864` up to and including `NNM001000874`
* This identifier is also found in the folder name (on the G Drive). For example, within the folder `MMNAT01_B1_F2_V3`, there is a subfolder `NNM001000864`. This folder "represents" the object. There are 11 such folders.
* Proceeings, being printed volumes, contain pages.
* Objects of the type `Proceeding` therefore have children. These are objects of the type (or subtype) `proceeding_page`
* These childen are files in the folders (on the G Drive). They have file names like `MMNAT01_AF_NNM001000864_001.jpg`. That is the first scan (first page) belonging to object `NNM001000864`.

MMNAT01_B1_F2_V3 | Subfolder name
----- | -----
Subfolder 1 | NNM001000864
Subfolder 2 | NNM001000865
Subfolder 3 | NNM001000866
Subfolder 4 | NNM001000867
Subfolder 5 | NNM001000868
Subfolder 6 | NNM001000869
Subfolder 7 | NNM001000870
Subfolder 8 | NNM001000871
Subfolder 9 | NNM001000872
Subfolder 10 | NNM001000873
Subfolder 11 | NNM001000874

<!-- * The table below shows a fourth subject, and I'm not sure in what volume(s) these Proceedings were published. -->
<!-- / P.W. Korthals. - 259 p., 70 pl. -->
<!-- 
Subject | Object | Metadata
----- | ----- | -----
Zoölogie | NNM001000864 | Deel I van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). 1839-1844.  Mammalia. Door P.v.Oort e.a.
Zoölogie | NNM001000865 | Deel II van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Aves/Reptilia/Pisces. Door S. Müller en H. Schlegel
Zoölogie | NNM001000866 | Deel III van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Insecta (door W. de Haan)/inhoud Zoologie
Botanie | NNM001000867 | Deel IV van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Tekst (P.W. Korthals)/inhoud Botanie
Botanie | NNM001000868 | Deel V van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Botanie: Platen
Land- en Volkenkunde | NNM001000869 | Deel VI van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Geredigeerd door J.A. Susanna, gedrukt door J.G. la Lau. Leiden. 1839-1844. Land- en Volkenkunde: Tekst. Door S. Müller
Land- en Volkenkunde | NNM001000870 | Deel VII van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Land- en Volkenkunde: Tekst/Inhoud Land- en Volkenkunde. Door S. Müller
Land- en Volkenkunde | NNM001000871 | Deel VIII van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Land- en Volkenkunde: Platen. Door P.v.Oort e.a.
Prospectus en nog deel van Zoologie en andere afleveringen… | NNM001000872 | Deel IX van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). 1839-1844. Zoologie: Prospectus/varianten modellen Mammalia. Door H. Schlegel e.a.
Prospectus en nog deel van Zoologie en andere afleveringen… | NNM001000873 | Deel X van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Zoologie: Varianten modellen Aves/Reptilia/Pisces/Insecta/Omslagen. Door A. Maurevert e.a.
Prospectus en nog deel van Zoologie en andere afleveringen… | NNM001000874 | Deel XI van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). Land- en Volkenkunde: Varianten/Modellen/Omslagen/Inhoud IX-XI. Door P.v.Oort e.a.
-->


### Object Type 2: Drawings

* Although drawings are found in the field notes and the Proceedings, these are not included in the object of the type `drawing`
* Object of the type `drawing` are found (on the G Drive) in the folders `MMNAT01_B2_F2_V3` and `MMNAT01_B3_F2_V3`
* Within the first, there are 10 subfolders and within the second, only one. I have no idea why there are two of these main folders instead of one. There are no folders within these subfolders. 
* Each folder contains files (scans). These "represent" objects of the type `drawing`
* It is unclear to me whether the folders themselves "represent" an object. Take a folder name like "NNM001000001-NNM001000100". does this represent 100 objects on a level higher than individual drawings? Do we have any information for these objects? If so, we call the objects of the type `drawing_group`.
* For example, object `NNM001001480` (type `drawing_group`) has three child objects (type `drawing`). These are repesented by three files (scans) with the names `MMNAT01_AF_NNM001001480_001.jpg`, `MMNAT01_AF_NNM001001480_002.jpg`, and `MMNAT01_AF_NNM001001480_003.jpg`
* There are some 1500 objects of the type (or subtype) `drawings`
* In some cases, files in the Drawings folders are doubles of files in the Proceedings folders. For example, `NNM001000868_011.jpg` and `NNM001000868_050.jpg`. Simply ignore this.

MMNAT01_B2_F2_V3 | Subfolder name
----- | -----
Subfolder 1 | NNM001000001-NNM001000100 
Subfolder 2 | NNM001000101-NNM001000200
Subfolder 3 | NNM001000201-NNM001000300
Subfolder 4 | NNM001000301-NNM001000400
Subfolder 5 | NNM001000401-NNM001000500
Subfolder 6 | NNM001000501-NNM001000600
Subfolder 7 | NNM001000601-NNM001000700
Subfolder 8 | NNM001000701-NNM001000800
Subfolder 9 | NNM001000801-NNM001000863
Subfolder 10 | NNM001001474-NNM001001487

MMNAT01_B3_F2_V3 | Subfolder name
----- | -----
Subfolder 1 | NNM001000722-NNM001000874

### Object Type 3: Handwritten

* The objects of the type `handwritten` represent handwritten material: (field) notes, lists, correspondence, etc.
* These objects sit in in two folders on the G Drive: `MMNAT01_B4_F2_V3` and `MMNAT01_B5_F2_V3`
* These folders contain numerous subfolders with names like `NNM001001032` or `NNM001001093-1276`. 
* These subfolders represent objects that **group** handwritten material. For example, `NNM001001061-1414`contains notes and letters by Boie.
* such groups have metadata, and therefore we need to have them as objects in their own right: objects of the type `handwritten_group`
* These groups contain subgroups: groups in their own right, such as a note book or a multiple-page letter. But we don't have metadata for them so can ignore them.
* The subfolders contain files (.jpg), with names like `MMNAT01_AF_NNM001001032_001.jpg` or `MMNAT01_AF_NNM001001093_001.jpg`
* These files represent objects of the type (subtype) `handwritten_page`. For example, `MMNAT01_AF_NNM001001032_001.jpg` represents an object of the (sub)type `handwritten_page`which has a further subtype (subsubtype) `Note` and is the first page (cover) of a note book that for which we have the following metadata `Cover, Kuhl – sketch book in fishes | Kartonnen omslag met bandjes; bevat twee bundels aantekeningen. Titel op zijkant: Kuhl Mammalia Aves. Titel op voorkant: Vogels en Zoogdieren Kuhl` (In this case the subgroup equals the group).
* Irritatingly, some groups contain only covers (like `NNM001001032`) and others only contents (like `NNM001001033`), and others both. So we can't know what cover belongs to what contents.

There are too many folders to create a table here.

### Other types of objects
NCO contains other types of objects, such as creator, place, species, and subject. A full overview is found [here](https://gitlab.com/brillpublishers/making-sense/blob/master/data_model/data_model_and_examples.md).

<!-- 
Notes Group Object | Description | Long Description
----- | ----- | -----
NNM001001032 | Cover, Kuhl – sketch book in fishes | Kartonnen omslag met bandjes; bevat twee bundels aantekeningen. Titel op zijkant: Kuhl Mammalia Aves. Titel op voorkant: Vogels en Zoogdieren Kuhl
NNM001001033 | (Field)notes by Kuhl on Mammals | Bundel aantekeningen: Mammalien. Bevat mapjes met de volgende titels: Chiropterae/Lemur volitans sijn. Galeopithecus variegatus Aud./Quadrumanae/Falculatae/Glires/Rumantia/Nantes/Edentuli/Anatomie des Elephas indicus fem. Buitenzorg 1821 d.16 februar/Pachydermata
NNM001001034 | (Field)notes by Kuhl on Mammals | Bundel aantekeningen: Voegel. Bevat mapjes met de volgende titels: Phaeton/Creagroura Nobis/Gallus/Palmipedes/Sterna/Ardea/Mycteria/Crex/Diplonura Nobis/Tringa/Grallae/Columba/Hirundo/Psittacus/Merops/Alcedo/Nectarinia/Picus/Sitta/Trogon/Cuculus/Tringilla/Sylvia/Muscicapa/Myothera/Turdus/Genera (+10 genusnamen)/Priolus/Buceros/Corvus/Edolius/Occypterus/Tycnonotus carphocephalus Nobis/Tycnonotus ruficeps Nob./Myotherae Cyanurae/Diplonura Nobis/Parus Pardalotus/Lanius/Gen. Pitta Vicill./Occypterus/Ceblepyris/(doorgestreept:) Falco javanicus/Falco palumbariaides(?)/Strix
NNM001001035 | (Field)notes by Kuhl | Kartonnen omslag met bandjes; bevat twee bundels aantekeningen en een stapeltje losse blaadjes. Titel op zijkant: Kuhl Amphibien. Titel op voorkant: Amphibien Kuhl
NNM001001036 | (Field)notes by Kuhl on Amphibians | Bundel aantekeningen: Amphibia. Bevat mapjes met de volgende titels: Rhacopherus Nobis/Ooeidozyga/Megophrys/Megophrys/Ooeidozyga/Rana/Rana/Gen. Caecilia Lin./Coluber/Naja Nobis/Dipsas/Bungarus Daudin Pseudoboa Oppel/Ptyscizoon(?) Nobis/Tufinambis Daudin/Tachydromus/Acrochordus javanicus/Agama/Chamaeleon/Gecko vittatus/Platydactylus Cuvier/Hemidactylus Cuvier/Gonydactylus Nobis/Trigonocephalus/Platydactylus Cuvier/Tubinambis Daudin/Python/Agama Daud/Gonyodactylus Nobis/Leptura Nobis/Tropinotus Nobis/Amblycephalus Nobis
NNM001001037 | (Field)notes by Kuhl on Amphibians | Bundel aantekeningen: Amphibia. Bevat mapjes met de volgende titels: Racophorus Nobis/Bufo/Bufo/Trionyx javanicus/Draco/Draco xanthogeneion/Hyla/Scincus/Scincus/Tachydromus/Homalopsis/Hemidactylus Cuvier/Draco/Typhlops Tortrix/Xenopeltis Reinw/Hyla/Trionyx javanicus/mapje met 3 losse blaadjes: Amphib. Kopf/Noctua macrops/Typhlops umbricalis(?) L./Typhlops/Kraspedocephalus Nobis/Tortix rufus/Dipsas Laurent. Cuvier/Tropinotus Nobis/Leptura Nobis/Crocodylus biporcatus/Tortricilus(?)/Buryororum(?) Cuv/5 lijsten met lengtematen en gewichten van ingewanden/Coluber picatus/Basiliscus amboinesis/6 Juli/Coluber erythrogaster/Coluber ? Nobis/Coluber cruciferus N./Coluber ?/Coluber/Coluber quadrijuger(?)/Agama/Bufo/Rana vittata/Lacerta agilis/Elaps Schneider/
NNM001001038 | (Field)notes by Kuhl | 1) mapje met titel Buxyarus Daudin Pseudoboa Oppel, 2) 7 losse blaadjes met soortnaam+beschrijvingen. Hydrophis granulatus/Hydrus spiralis/Coluber atrofuscus/Coluber interruptus Oppel/Coluber taeriolatus/Col cerberus Daud/Coluber naja L.
NNM001001039-1040 | (Field)notes by Kuhl | Kartonnen omslag met bandjes; bevat 49 bundeltjes aantekeningen. Titel op zijkant: Kuhl Anatomie. Titel op voorkant: Anatomie der Gewirbelten Thiere Kuhl
NNM001001040 | (Field)notes by Kuhl | Bundel aantekeningen. Bevat mapjes en losse blaadjes. Mapjes hebben de volgende titels: Pastor cristatellus/Carbo javanicus/Pelicanus/Anas rufiventris/Sterna melanocephalo/Charadrius affinis/Tringa/Crex(?)/Ardea/Kreagroura/Sylvia/Myothera/Muscicapa/Turdus/Oriolus/Alcedo/Tringilla/Sitta/Lanius/Hirundo/Nectarinia/Dicaeum Cuvier/Ceblepyris/Buceros/Corvus/Edolius/Gallus/Columba/Picus/Cuculus/Bucco/Trogon/Psittacus/Strix/Falco/Quadrumanae/Mammalien/Pachydermata/Anatomie der Testudo mydas von den Cocosinseln/Nectris puffinus/Sula minor/Tatanus ?/Limosa ?/Namenicus ?/Parra indica/2 mapjes zonder titel

The reamining Notes Group Objects contain too many items to be included meaningfully in a table, so I left them out.
-->

<!-- 
NNM001001041-1042 | Kuhl – aantekeningen 

1) Kartonnen omslag met bandjes; bevat bundeltjes, losse en dubbelgevouwen blaadjes met aantekeningen. Titel op zijkant: Kuhl Pisces. Titel op voorkant: Fische I, II, III, IV Kuhl

2) Bundel aantekeningen. Bevat (in de volgende volgorde): stapeltje losse en dubbelgevouwen blaadjes met (overgeschreven) aantekeningen over de anatomie van vissen, mapjes met bundels aantekeningen met de volgende namen op het voorblad: Gadus/Cyprini/Muraena/Pleuronectes/Ophicephalus/Gymnotus/Macrourus/Lepidoleprus/Lepadogaster/Cyclopterus/Echeneis/Ordo Lophobranchii/Salmo/Balistes/Seyllium/Squalus/Chimaera/Accipenser/Polyodon Spatularia/Diodon/Tetrodon/Orthagoriscus/Ostracion/(doorgestreept:) Labrus/Anatomie der Scomberarten/waarnemingen over de anguilla electrica/2 mapjes+los blaadjes met aantekeningen/Silurus/23 losse en dubbelgevouwen blaadjes met aantekeningen/5 lijsten met lengtematen en gewichten van ingewanden/losse blaadjes

Directory NNM001001043-1045 Kuhl – fische

1) Kartonnen omslag met bandjes; bevat bundeltjes aantekeningen. Titel op zijkant: Kuhl Pisces. Titel op voorkant: 2. Fische Kuhl

2) Bundel aantekeningen. Bevat (in de volgende volgorde) mapjes met titels: Fische (aantekening: Manuscripten van de leden der Natuurk. Commissie na den dood van Forsten overgezonden)/Fische/Anatomie der Carcharien/Leptopcephalus/Ammodytes/Ophidium/Callionymus/Trichonotus/Gomephorus/Labroides/Anatomie der Scomberarten/Sillago/Sparoides/Percae/Taemanotus diacanthus/Scorpaena scrofa/Inermes/Dentes capensis Nobis/Dentices/Microdonti/Scombroides/Scombroides/Squamifermes/Tistularia (los blaadje)/Blemniorum/Lobiordiarum(?)/Gobius/mapje zonder naam/Acanthopterygii Percae Sparoides/Acanthopterygii Percae Percae/

3) Los stuk karton met de tekst 'in de maag van Charadricus affinus vond ik ?'

Directory NNM001001046-1399 Kuhl / Van Hasselt


1) voorblad testament J.C. van Hasselt, 2) testament J.C. van Hasselt (met lakzegel), 3) Kopie testament J.C. van Hasselt
2) Brief Van Hasselt aan C.J. Temminck. Batavia, 15 februari 1822
3) Catalogus reptielen Kuhl & v.Hasselt. Verzonden in april 1826
4) Catalogus vissen Kuhl & v.Hasselt
5) Brief van H. Kuhl(?) aan ? Frans/Duits


Directory NNM001001051-1059 

1) Bundel aantekeningen. Aves Granivorae. Bevat mapjes met aantekeningen over de volgende genera: Fringilla/Vinago/Columba/Alauda/Phasianus/Perdix


Directory NNM001001052-1060 - notes probably by Boie 

1) Bundel aantekeningen. Aves Insectivorae. Bevat mapjes met aantekeningen over de volgende genera: Buceros/Trichophorus/Phyllornis/Phoenicornis/Jora/Edolius/Sylvia/Lanius/Pardalotus/Enicurus/Muscicapa/Matacilla/Pitta/Eurylaimus/Pyctonotus/Orthotomus/Pomatorhinus/Napothera/Timalia/Myiophoneus/Myiothera/Ripidicala/Ocypterus/Malurus
2) Bundel aantekeningen. Aves Zygodactylae. Bevat mapjes met aantekeningen over de volgende genera: Centropus/Cuculus/Phoenicophaeus/Cocoyzus/Trogon/Psittacus/Picus/Bucco
3) Bundel aantekeningen. Aves Inisodactylae & Chelidones. Bevat mapjes met aantekeningen over de volgende genera: Diaceum/Alcedo/Alcyon/Hirundo/Dendrochelidon/Nectarinia/Merops/Podargus/Rupicola/Caprimulgus
4) Bundel aantekeningen. Aves Omnivorae. Bevat mapjes met aantekeningen over de volgende genera: Pastor/Lamprotornis/Corvus/Glaucopis/Gracula/Oriolus/Coracias
5) Bundel aantekeningen. Amphibia. Bevat mapjes met aantekeningen over de volgende genera: Serpentes/Cheloniae & Batrachi/Saurii
6) Bundel aantekeningen. Aves Rapaces. Bevat mapjes met aantekeningen over de volgende genera: Ornithologie im Allgemeinen/Falco/Strix
7) Bundel aantekeningen. Aves Grallae & Palmipedes. Bevat mapjes met aantekeningen over de volgende genera: Scolopax/Ibis & Tantalus/Totanus & Tringa/Pelecanus/Gallinula/Ardea/Porphyris/ Ciconia/Anas/Sterna/Plotus/Charadrius/Acanthocarpus
8) Kartonnen omslag met bandjes; bevat 1 bundel aantekeningen. Titel op zijkant: Boie Mammalia Aves Reptilia. Titel op voorkant: Hendrik Boie's zoologische Aanteekeningen uit den Indischen Archipel. Mammalia, Aves, Reptilia 1826 en 1827
9) Boie: Bundel aantekeningen met op omslag 'Oiseaux pelagiens par Boié'. Aantekeningen in het Frans en Duits


Directory NNM001001061-1414 – notes and letters by Boie

1) Boie: Bundel aantekeningen. Mammalia. Bevat mapjes met de volgende titels: Mammalia/Chiroptera
2) Boekje met aantekeningen (juni-november 1826)
3) Persoonlijke aanvullingen op het boek van Wolf & Meijer (Nachtraege zum ornithologischen Taschenbuch von Wolf & Meijer)
4) Omslag met vrijwel onleesbare tekst. 7 februari 1831. Bevat documenten aangaande het overlijden van H. Boie, boedelrekeningen en correspondentie
5) Verklaring van overlijden van H. Boie. Batavia, 6 sep 1827. Aanvaarding door H. Macklot en G. van Raalten
6) Rekeningen, boedelrekeningen en nota's H. Boie
7) Catalogue des mamiferes & oiseaux. Boie & Macklot, april 1826. 1e bezending K. de GH, ontvangen in October 1826
8) Omslag: Dr H. Boie. Bevat brieven van H. Boie aan verschillende personen (o.a. J.A. Susanna, C.J. Temminck)
9) Brief van August Becker. Leipzig, 16 sept 1823

Directory NNM001001070-1473 Diard brieven en ornithologische aantekeningen

1) Brief aan C.J. Temminck. Buitenzorg, 21 jul 1820
2) Omslag met tekst 'Diard' en tekening van doodshoofd
3) Omslag met tekst 'ornithologische aanteekeningen van Diard'
4) Losse bladen met ornithologische aantekeningen van P.M. Diard
5) Objectenlijst. Catalogue des objets d'hist nat, poissons e reptiles envoyés par mr. v. Raalten. Aout 1826. Voor ontvangst getekend door H. Schlegel


Directory NNM001001075-1081 Reisverhaal Dr. Forsten => Menado + Brieven

1) Reisverhaal naar Menado. Dr. E.A. Forsten
2) Kort verslag van het in Indië verrigte van december 1838 tot april 1840
3) Reisverslag 1840-1842
Directory NNM001001078-1472 - Forsten 

1) Brief Forsten aan H. Schlegel. Helvoet, 2 aug 1838
2) Benoeming van E.A. Forsten als lid van de Natuurkundige Commissie: Extract uit het Register der Besluiten van den Gouverneur Generaal van Nederlandsch Indië. Buitenzorg, 22 december 1839 + 2 bijlagen Instructie
3) Diverse (gebruiks)aanwijzingen, recepten, aantekeningen, berekeningen, tabel temperatuur/barometerstand
4) Mapje met aantekeningen 'Dieren waargenomen op de zeereis in 1838 van Holland naar Java' + 2 losse blaadjes en 1 tekening van kwal
5) Bundeltje aantekeningen over planten (o.a. Rafflesia)
6) Lijsten met metingen (Lengte- en breedtetafel van Eilanden etc.)/Tafelen van de Zons Declinatie op den middag(?) van Teneriffa voor de jaren 1809-1812/Verscheide stellingen over de wording der Aarde en hare veranderingen/Praktische regel om de dikte eens booms op eene gegevene hoogte te bepalen/Instructies van het museum medegekregen: over het verzamelen van Mollusca op het strand
7) Diverse (onkosten)rekeningen, kwitanties, declaraties, nota's
8) Verzamelde vogelen van het eiland Amboina
9) Soortenlijst 3 mei - 20 oktober ?, 1e bezending


Directory NNM001001087-1238 – Horner 

1) Aantekenboekje over mineralen. Frans/Nederlands
2) Brief aan H.Schlegel(?). Hramat bij Batavia, 27 feb 1836
3) Dagboek Ludwig Horner, bijgehouden op Sumatra. Bestaande uit een voorblad en 6 bundels (genummerd Sumatra 1 t/m 6). Tekst op voorblad: Journal van Dr. L. Horner door hem gehouden gedurende zijn verblijf op Sumatra. NB Horner's dagboeken over zijne reizen op Java en Borneo schijnen bij zijnen dood te Padang, ontvreemd gevonden te zijn. Dr. S. Müller. Für das Reichs-Museum in Leijden

Directory NNM001001090-1418 - Macklot

1) Stukje papier dat het testament en boedelrekening van H. Macklot bijeenhoudt, met de tekst 'dit hier(?) blijft tot verantwoording van de afgerekende boedel, tot(?) zoverre, bij den Heer Muller berusten
2) Testament H. Macklot
3) Boedelrekening H. Macklot. Getekend door o.a. S. Müller, Buitenzorg 1 dec 1833

Directory NNM001001093-1276 – letters by Junghuhn / Korthals / Schwaner

1) Junghuhn: Brief aan C.J. Temminck. Den Haag, 18 feb 1849 (2 brieven)
2) Korthals: Brief aan C.J.Temminck. Buitenzorg, 18 apr 1830
3) Schwaner: Brief aan J.A. Susanna. Helvoetsluis, 26 apr 1842

Directory NNM001001096 - Mueller - zoological (field)notes

1) Kartonnen omslag met bandjes; bevat 4 bundels aantekeningen en een mapje met losse velletjes. Titel op zijkant: S. Müller Aves. Titel op voorkant: Salomon Müller's zoologische Aanteekeningen uit den Indischen Archipel. Aves I. Rapaces, Omnivorae, Anisodactylae, Zygodactylae, Chelidones. 1826-1837

Directory NNM001001097 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Aves Rapaces. Bevat mapjes met de volgende titels: Genus Strix Linn/Subgenus Haliaetus/Genus Falco Linn. + losse blaadjes met aantekeningen

Directory NNM001001098 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Anisodactylae & Chelidones. Bevat mapjes met de volgende genusnamen: Hirunda/Nectarinia/Dicaeum, Pardalotus/Sitta/Melliphaga/Alceda, Dacela/Merops/Phyllornis/Epimachus/Rupicola/Caprimulgus, Padangus

Directory NNM001001099 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Zygodactylae. Bevat mapjes met de volgende genusnamen: Psittacus/Bucco/Phaenicophaus, Cintropus, Bubutus, Calobates, Cuculus/Picus/Trogon/Picumaus


Directory NNM001001100 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Aves Omnivorae. Bevat mapjes met de volgende genusnamen: Oriolus, Paradisea, Artamia/Ixos/Gracula/Garrulus, Corvus/Barita, Vanga/Pastor/Lamprotornis/Buceros

Directory NNM001001101 – Mueller - zoological (field)notes

1) losse velletjes met aantekeningen over vogels. In mapje zonder titel

Directory NNM001001102 – Mueller - zoological (field)notes

1) Kartonnen omslag met bandjes; bevat 17 bundels aantekeningen. Titel op zijkant: S. Müller Aves. Titel op voorkant: Salomon Müller's zoologische Aanteekeningen uit den Indischen Archipel. Aves II. Insectivorae, Granivorae, Gallinaceae, Palmipedes, Grallae. 1826-1837

Directory NNM001001103 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Aves Grallae & Palmipedes. Bevat mapjes met de volgende genusnamen: Glareola, Scalapax, Rhynchaea/Numerius, Tatanus, Tringa, Charadrius, Tantalus, Haematopus, Oedicnemus, Himantopus/Ardea, Cicania/Sterna/Diomedea, Procellaria/Porphyria, Gullinula, Parra, Fulica/Anas

Directory NNM001001104 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Aves Gallinaceae. Bevat mapjes met de volgende genusnamen: Edolius, Ceblepijris/Columba/Polyplectron/Megapendius/Megapodius/Pava, Gallus, Argus, Lophophorus

Directory NNM001001105 - Mueller - zoological (field)notes

1) Bundel aantekeningen: Aves Insectivorae. Bevat mapjes met de volgende genusnamen: Kitta, Irena, Sphecatheros/Acijpterus/Saxicola, Anthus, Celanda(?)/Timalia, Garrulax/Rhijsidura, Mijiolestes/Malurus/Mijiophoneus/Myiothera, Eupetes/Pitta/Phoenicorais/Parus/Coracias/Hijlorharis(?), Napothera, Drijmophila/Matacilla, Enicurus/Sylvia, Hylophila, Fora, Orfothomus(?)/Turdus/Muscicapa/Pomatorhinus/Lanius

Directory NNM001001106 – Mueller - zoological (field)notes

1) Bundel aantekeningen: Aves Granivorae. Bevat mapjes met de volgende genusnamen: Fringilla/Glaucapis

Directory NNM001001107-1112 Mueller – zoological (field)notes

1) Kartonnen omslag met bandjes; bevat 6 bundels aantekeningen. Titel op zijkant: S. Müller Mammalia Reptilia. Titel op voorkant: Salomon Müller's zoologische Aanteekeningen uit den Indischen Archipel. Mammalia et Reptilia.1826-1837
2) Bundel aantekeningen: Marsupialiea & Edentatea. Bevat losse blaadjes met aantekeningen
3) Bundel aantekeningen: Insectivorea. Bevat losse blaadjes met aantekeningen
4) Bundel aantekeningen: Quadrumana. Bevat mapjes met de volgende genusnamen: Hama, Simia, Hylobates/Jannus(?), Cercopithecus, Stenops/Semnopithecus
5) Bundel aantekeningen: Carnivora. Bevat mapjes met de volgende genusnamen: Canis, Arctitis, Mijdaus/Mustelus, Viverra, Herpestes/Felis
6) Bundel aantekeningen: Herbivora, Frugivora. Bevat mapjes met de volgende genusnamen: Pachijdermea/Herbivorea/Glires


Directory NNM001001113 - Mueller

1) Bundel aantekeningen: Amphibia. Bevat mapjes met de volgende genusnamen: Batrachier/Scincus/Draco, Calotes, Logrhijnus, Basilisius/Lophias, Bungarus, Naja, Elaps/Testudiaes/Coluber, Tropidanotus, Dipsas/Acantius/Allgemein/Varanus, Tachijdromus, Cordijlus, Crocodilus, Tropisaura/Geckones/Brachijurus, Lijcodon, Amblijcephalus/Drijopis, Dendrophis/Pijthon. + losse velletjes met aantekeningen

Directory NNM001001114-1454 - Mueller 

1) Catalogus der afbeeldingen van zoogdieren en vogels, welke door de Natuurkundige Commissie in Nederlandsch Indie met Z.M.Korret Nahellenia naar Nederland zijn gezonden (Buitenzorg, 20 mei 1830)
2) Lijst der teekeningen van zoogdieren, vogels, reptilien en visschen, welke met de bezending der voorwerpen van Natuurlijke Geschiedenis, gemerkt M, van Java naar Nederland zijn verzonden, door de Natuurk. Commiss (Buitenzorg, 1 feb 1831)
3) Voorblad voor catalogi Salomon Müller, 1826-1837


Directory NNM001001117 -- vanaf plaatje 15 - Aantekeningen gedurende eene reis door de Praenger Regentschappen / Krawang – P. Van Oort! 

1) Dagboek P.v.Oort. Java, 1831-1832 (deel I)

Directory NNM001001118 – diary P.v. Oort 

1) Dagboek P.v.Oort. Java, 1831-1832 (deel II). Bevat bundels met de volgende titels: 1) Aanteekeningen gehouden op eene reize over Java (1831), 2) Togt naar den Salak, 3) Aanteekeningen, gehouden gedurende eene reize over Java (1832), 4) Aanteekeningen gehouden op eene reize over het eiland Java door het lid der Natuurkundige Commissie P.v.Oort (1833), 5) 6 bundels Aanteekeningen gehouden op eene reize over Java en naburige eilanden, vervolg van den laatsten december 1832

Directory NNM001001119 – ook nog aantekeningen Oort - maar stadium eerder. 

1) Bundeltjes aantekeningen P. v. Oort


Directory NNM001001120-1121 – Kuhl – field (notes)  

1) Kartonnen omslag met bandjes; bevat 7 bundeltjes aantekeningen
2) Bundel aantekeningen. Bevat mapjes met de volgende titels: Beobachtungen zur See von Holland bij Indies(?)/Annelides/Radiarii/Mollusca/Crustacea/Arachnides/Insecta

Directory NNM001001122-1131 – Diard

1) Blauwe omslag met opschrift Aves. Bevat losse velletjes aantekeningen, losse tekeningen en 6 ingepakte bundeltjes met tekeningen (Echassiers/Zygodactyli/Oiseau de Proie/Varia(?)/Omnivores/Alciones)
2) Blauwe omslag met opschrift Mammalia. Bevat 2 bundeltjes met mapjes en losse velletjes aantekeningen en tekeningen: 1) vleermuizen etc., 2) 'Catalogus Raffleseanus' met tekeningen hert, catalogus Sumatra en Java
3) Blauwe omslag met opschrift Insecta. Bevat 6 blauwe mapjes met aantekeningen. Titels: Lepidopteres ?/Hymenoptères/Neuroptères/Hemiptères/Colioptères/Orthoptera
4) Blauwe omslag met opschrift Aptères. Bevat mapjes met tekeningen, losse blaadjes met vogelnamen en aantekeningen
5) Blauwe omslag met opschrift Molusques. Bevat mapjes (Pteropodes/Cephalopodes/Gasteropodes/Mollusques/Acephales) met aantekeningen en tekeningen
6) Blauwe omslag met opschrift Crustacea. Bevat aantekeningen en tekeningen
7) Blauwe omslag (zonder titel). Bevat tekeningen en aantekeningen over kikkers/padden
8) 9 mapjes met tekeningen+beschrijving. Titels: Cynthia/Timoriena/Pterotrachea/Monophora/Sternaspis/Pyrosoma/Boltenia/Brachiopodes/Nereis
9) 11 blauwe omslagen met aantekeningen en tekeningen over reptielen en amfibieën (o.a. Boa, Homalopsis, Ceratophrys, Serpentes grèle)
10) Diverse losse bladen met aantekeningen, tekeningen en kladversie van brief


Directory NNM001001132 – Diard – drawings and notes

1) 9 bundels aantekeningen over vissen. Titels: Malacopterijgiens A fam.1-3 en 5-6, B fam 1-3, C Apodes

Directory NNM001001132-1141 – Diard – drawings and notes 
Directory NNM001001133 – Van Oort

1. Bundel aantekeningen over vissen. Titel: Acanthopterijgiens. Famille Pectorales ?
2. Bundel aantekeningen over vissen. Titel: 3. Famille Scienes
3. Bundel aantekeningen over vissen. Titel: Fische
4. Bundels aantekeningen over vissen met op omslag geslachtsnamen
5. Bundel aantekeningen over vissen. Titel: Famille Sparoides
6. Bundel aantekeningen over vissen. Titel: Famille Pereoides
7. Bundel aantekeningen over vissen. Titel: 2, Famille ajones ?
8. Museumannalen XIV pag 184 (vissen)


Directory NNM001001142-1348 – part of diary Van Oort

1. Dagboekaantekeningen Sumatra (pagina 569 t/m 688)
2. Levensschets van H.C. Macklot, door P.v. Oort. Met begeleidende brief aan J.A. Susanna, 13 februarij 1834

Directory NNM001001144-1172 – Forsten – notes: zoogdieren, vogels 

1. Kartonnen omslag met bandjes. 'Zoogdieren. Vogels. Amphibien. Geologie. Forsten'

Directory NNM001001145-1168 – various notes – nice drawings 

1. Mapje met aantekeningen: Ordo XIV Pinnatipedes
2. Mapje met aantekeningen: Mammalia. Ord II Quadrumana
3. Mapje met aantekeningen: Ferae
4. Mapje met aantekeningen: Fam 20 Subungulata
5. Mapje met aantekeningen: Fam 19 Duplicidentata
6. Mapje met aantekeningen: Fam 18 Aculeata
7. Mapje met aantekeningen: Fam 17 Palmipedia
8. Mapje met aantekeningen: Fam 15 Macropoda
9. Mapje met aantekeningen: Cunicularia Braute(?)
10. Mapje met aantekeningen: Fam 13 Sciurina
11. Mapje met aantekeningen: Ordo V. Marsupialia
12. Mapje met aantekeningen: Mammalia. Ord III Chiroptera
13. Mapje met aantekeningen: Mammalia Ord IV Ferae
14. Mapje met aantekeningen: Mammalia Ordo V Glires
15. Losse velletjes met aantekeningen: Pteropus, Phalangista, etc.
16. Mapje met aantekeningen: Ordo XI Alectorides
17. Mapje met aantekeningen: Ordo VI Anisodactyli
18. Mapje met aantekeningen: Ordre VII Alciones
19. Mapje met aantekeningen: Ordre VIII Chelidones
20. Mapje met aantekeningen: Ordo X Gallinae
21. Mapje met aantekeningen: Ordo XI Columbae
22. Mapje met aantekeningen: Ordre IV Granivores
23. Mapje met aantekeningen: Ordre II Omnivores
24. Mapje met aantekeningen: Ordre I Rapaces
25. Mapje met aantekeningen: Ordo XV Palmipedes
26. Mapje met aantekeningen: Ordre III Insectivores
27. Mapje met aantekeningen: Ordre XIII Grallatores

Directory NNM001001172-1178 – various notes – nice drawings

1. Mapje met aantekeningen: Ordre V Zygodactyli
2. Mapje met aantekeningen: 3e Division Antorus
3. Mapje met aantekeningen: 9) Columbae genre Columba
4. Mapje met aantekeningen: 2) Omnivores Genre 12 Ptilonorhynchus
5. Mapje met aantekeningen: zonder titel (over o.a. Caracara, Falco)
6. Mapje met aantekeningen: Coronellae
7. Aantekeningen geologie (Forsten)

Directory NNM001001179-1472 – Correspondence Van Hasselt/Boie/Temminck

1. Brief aan ?. Utrecht, 27 feb 1825
2. Brief. Voorzijde 'Diard', binnenzijde brief (klad)
3. Verantwoording B.N. Overdijk (1830) voor zijne personeele uitrusting naar Oost-Indië. Bevat 26 kwitanties
4. De Haan => Brief aan P.W. Korthals. Leiden, 20 dec 1832
5. Brief aan Zijne Excellentie te Brussel. Leiden, 31 mrt 1827 (2 bijlagen). Frans/Nederlands
6. Van Hasselt -> Brief aan C.J. Temminck. Geen datum in aanhef, wel een gedateerd postscriptum (29 oktober 1822)
7. Van Hasselt => Brief aan C.J. Temminck. 29 oktober 1822
8. Van hasselt => Brief aan dhr. Bayer(?). Bantam, 25 december 1822
9. Van Hasselt => Brief aan C.J. Temminck. Buitenzorg, 30 juli 1822
10. Catalogus van de 'Eerste Bezending der Eerste Afdeling', door v.Hasselt verzonden in december 1822
11. Omslag met tekst 'Correspondentie met den Hr van Hasselt te Batavia. 1823 en 1824'
12. Hasselt, Brief aan ?. Anjer, 1 apr 1823
13. Van Hasselt => Brief aan C.J. Temminck. Anjer, 1(?) mei 1823
14. Brief aan C.J. Temminck. Mei 1823
15. => Uittreksels van zich te A'dam (Artis) bevindende brieven (Kuhl and Van Hasselt) 
16. Boie => Brief aan L.F. Theyssens. Paris, 2 mei 1824
17. Boie => Brief van August Becker. Leipzig, 26 april 1825
18. Brief van ?. 29 nov 1825
19. Brief aan C.J. Temminck. Rhede vor Helvoet, 9 dec 1825
20. Brief aan J.A. Susanna van H.Macklot en H.Boie. Dijkzigt, 20 maart 1826
21. Boie: Brief aan C.J. Temminck. Kaap de Goede Hoop, 10 april 1826
22. Boie Brief aan C.J. Temminck. Buitenzorg, 22 juni 1826
23. boie: Brief aan J.A. Susanna. Buitenzorg, 23 juni 1826

Directory NNM001001202-1212 – correspondence Boie / Raalten / Forsten

1.  Boie to Temminck and Susanne => until: …1213.jpg. 
2.  Raalten to Lidth de Jeude => until: …1223.jpg
3.  Forsten to Susanna => until: …1236.jpg
4.  Horner to Temminck => until: …1238.jpg
5.  Junghuhn to Temminck => until …1259.jpg
6.  Junghuhn to others => until …1264.jpg
7.  Korthals to Temminck / Schlegel => until …1267.jpg
8.  Diard / Schwaner to Temminck / Susanna => until … 1276.jpg
9.  Macklot to Temminck / Susanna => until … 1283.jpg
10. Diard / Macklot to Susanna / Temminck => until 1332.jpg. 

Directory NNM001001333-1347 – correspondentie Samuel mueller

1. Mueller to Susanna/Boie/Temminck => until …1347.jpg
2. letters by: Van Oort to Susanna and others  until …1366.jpg
3. Diard => Temminck: Uittreksel van geschreven stuk van Gouvernement der Ned. Ind. 8 jun 1827 and other letters until … 1380.jpg.
4. Catalogus van natuurhistorische objecten Borneo, Cambodja, Malakka, Sumatra, gestuurd door Diard. Aangekomen maart 1830, getekend door H. Schlegel
5. also brieven korthals => de haan 
6.  Bundel aantekeningen: Amphibiae Kuhl 16. Bevat aantekeningen over Hemidactylus, Ptyodactylus, Gymnodactylus, Scincus, Batrachia, Typhlops, Hyla, Bufo, Bombinator, Chelonia, Emys, Ascalobates, Zygodactili
7. Bundel aantekeningen: 3e famille Serpens terrestres. Bevat aantekeningen over Coronella, Xenodon, Lycodon, Coluber, Herpetodryas, Psammophis
8. Bundel aantekeningen. Tekst op voorzijde: 'Manuscripten van de Leden der Natuurkundige Commissie, na den dood van Forsten overgezonden’. Bevat aantekeningen over Fortrix, Calamaria, Dendrophis, Dryiophis, Dipsas
9. Omslag met tekst 'Kuhl & v.Hasselt'
10. Twee pagina's (dubbelzijdig beschreven) met aantekeningen over Anguis, Caecilia, Typhlops, Eryx + verwijzingen naar naslagwerken
11. Losse pagina met tekst 'Eenige onvolledige Teekeningen welke door het ontijdig afsterven van den Heer van Hasselt niet zijn afgewerkt, wellicht zijn er nog eenige nieuwe spectien bij [etc]'
12. Dubbelgevouwen blad met op voorkant een zendingslijst
13. Soortenlijst ('Uit Kuhl's verzameling toegenomen door de waarde van f20'). W. de Haan 31 Dec. 1823
14. Soortenlijst voorwerpen ontvangen van den Heer Boie uit de verzameling van Kuhl'
15. Soortenlijst 'Aves javanicae ex itinere Kuhl & van Hasselt'
16. Zendingslijst 'Kuhl & van Hasselt 1824. Nota der Naturalien welke naar het Koninklijk Museum worden overgezonden. Kopy 4e bezending Duplicaat'
17. Zendingslijst 'Kuhl & van Hasselt. Kopij 5e bezending. Nota der voorwerpen van Natuurlijke Historie welke aan het Koninklijke Museum worden toegezonden'
18. Nota der kisten inhoudende voorwerpen van natuurlijke historie welke naar het Nederlandsch Museum worden vergezonden (zonder datum)
19. Inventaris der gelden, goederen en boeken nagelaten door wijlen Dr. Heinrich Boié overleden te Buitenzorg
20. Extract uit de vendu-rol der gehouden publieke verkooping te Buitenzorg op den 26 September 1827 voor rekening des boedels wijlen Dr. Heinrich Boié door Dr. Heinrich Macklot
21. Brief van de ontvanger van den Inpost der Collaterale Successiën (J. van Slingerlandt), aan G. van Raalten. Batavia, 13 okt 1827
22. Brief van dhr. Launy aan H. Macklot. Buitenzorg, 5 nov 1827
23. Brief van de Secretaris der Bataviasche Weeskamer aan H. Macklot. Batavia, 1 nov 1827
24. Goederen uit de nalatenschap van H. Boie, toekomende aan mevrouw Boie
25. Bevelingsbrief van de Fiskaal bij den Raad van Justitie te Batavia aan H. Macklot. Batavia, 14 dec 1829
26. Verklaring door P.-M. Diard en P. van Oort, gericht aan de Assistent-Resident, over taxatie van boeken wijlen H. Boie. Buitenzorg, in januari 1830 (plus kopie)
27. Brief aan H. Macklot van de Algemene Secretarie. Batavia, 15 feb 1830. Over te betalen belasting over nalatenschap H. Boie
28. Brief aan G.-G. van Nederlandsch Indië, van H. Macklot. Buitenzorg, 18 mrt 1830. Over te betalen belasting over nalatenschap H. Boie (+kladbrief)
29. Brief aan H. Macklot, van de Adjunct-Secretaris van de G.-G. van Nederlandsch Indië. Batavia, 3 mei 1830
30. Ontvangstbewijs, getekend door J. van Slingerlandt. Batavia, 18 mei 1830. Belasting over boedel H. Macklot betaald door H. Macklot
31. Kopie van verklaring van erfeniswaarde H. Boie (Batavia, 18 mei 1830) + kopie testament H. Boie
32. Lijst der boeken van wijlen Dr. Boie
33. Testament H. Boie
34. Nota, gericht aan H.Macklot + losse velletjes met aantekeningen
35. Lijst met Latijnse en lokale soortnamen (Timor)
36. Lijst met diernamen van Sumatra
37. Lijst met Maleysische diernamen van Sumatra
38. Catalogus Chiroptera (verzonden naar Holland 24 jan 1828)
39. Catalogus (kopie) Sumatraanse vogels (verzonden naar Holland 24 jan 1828)
40. Catalogus zoogdieren Nieuw-Guinea
41. Catalogus vogels Nieuw-Guinea
42. Catalogus reptielen Nieuw-Guinea
43. Lijst van de op Amboina verkregen zoogdieren
44. Catalogus vogels Amboina
45. Catalogus vogels, verzonden van Amboina naar Buitenzorg. 20 apr ?
46. Catalogus amphibiën Amboina
47. Catalogus vogels Banda
48. Pagina met namen van soorten verzameld door S. Müller op Banda (volgens de Verhandelingen) en vogels aanwezig van S. Müller van Banda in het museum
49. Catalogus zoogdieren Pulu Samao
50. Catalogus vogels (huiden+skeletten) op Pulu Samao
51. Catalogus zoogdieren (huiden+skeletten)
52. Vogelhuiden van Attapoepoe (Timor), maart en april 1829
53. Vogelskeletten van Timor
54. Catalogus der door de Natuurkundige Kommissie in Nederlandsch Indiën met Z.M. Fregat Belona in de maand februarij 1830 naar Nederland verzondene voorwerpen van natuurlijke historie
55. Catalogus van de Natuurkundige Kommissie in Nederlandsch Indie in den maand februarij 1831 naar Nederland verzonden voorwerpen van natuurlijke historie. Verzending L
56. Catalogus van de Natuurkundige Kommissie in Nederlandsch Indie in den maand februarij 1831 naar Nederland verzonden voorwerpen van natuurlijke historie. Verzending M
57. Catalogus van de Natuurkundige Kommissie in Nederlandsch Indie in den maand februarij 1831 naar Nederland verzonden voorwerpen van natuurlijke historie. Verzending N
58. Catalogus voorwerpen verzonden van Padang naar Rotterdam (aug 1835)
59. Eerste zending van Sumatra (Catalogus voorwerpen verzonden van Padang naar Rotterdam, aug 1835). Bezending A
60. Catalogus voorwerpen verzonden met schip Christina Bernardina, van Padang naar Amsterdam, oktober 1835. Bezending B (+kopie)
61. Catalogus derde bezending. November 1835, van Padang naar Rotterdam. Bezending C
62. Catalogus vierde bezending. December 1835, van Padang naar Amsterdam. Bezending D (+kopie)
63. Lijst der voorwerpen van Natuurlijke Historie, welke de Natuurkundige Commissie in de maand April 1836 aan het Museum te Batavia heeft afgegeven + kwitantie (2000 gulden)
64. Vijfde bezending van Sumatra. Bezending E, mei 1836
65. Vijfde bezending naar Leijden (mei 1836)
66. Overzicht van alle op Sumatra verzamelde dieren
67. Lijst van alle door ons op Borneo verkregene vogelen
68. Lijst der voorwerpen welke de N. Commissie in maart 1837 aan het Bataviasche Museum heeft afgegeven. Bevattende dieren van Borneo
69. Bezending X.Y.Z. uit Java verzonden door den Heer Muller
70. Lijst van zoologische resultaten, verzameld tijdens het 11-jarig verblijf van dhr. Müller in Indië (eerst samen verzameld met de heren Boié en Macklot, daarna alleen)
71. Overzicht zoologische resultaten S. Müller (kladversie van NC_a_Mu_035_036, uitgesplitst naar vindplaats)
72. Losse pagina met aantallen verzamelde dieren, uitgesplitst naar Borneo/Sumatra/Buitenzorg/Java/N.Guinea/Timor
73. Catalogus der Zoogdieren, Vogelen en Reptilien van Java
74. Mineralen van Makassar, Bantam, Amboina & Banda
75. Soortenlijst planten. Tondano, 19 jun 1840. Nr.1-178
76. Soortenlijst Celebes (Menado) Forsten
77. Los vel met lijst van vogels
78. Mammalia, derde bezending
79. Tweede verzameling, 20 jun 1841. Nr.1-126
80. Lijsten der inhoud der vaten en kisten gemerkt NK van Nr.I-XII, verzonden met het schip Bonjol, kapitein C. C. Hansen. Manado den ? Mei 1841, Het Lid der Natuurkundige kommissie. Bijlage 2
81. Lijsten der verzamelde dieren, mineralen enz. in de Manahassa Manado en verzonden met de Bonjol, kapt C. C. Hansen naar Europa. April 1841, Forsten
82. Soortenlijst Forsten
83. Twee losse vellen met soortenlijsten
84. Los vel met soortenlijst
85. Los vel met soortenlijst
86. 'Lijst van dieren door mij op Java verzameld'. Bijlage 1
87. Soortenlijst
88. Lijst van vogelen op Java en Sumatra + lijst van zoogdieren
89. Soortenlijst Reptilien van Java
90. Soortenlijst
91. Temminck => Brief aan P. Diard (19 aug 1830). Frans
92. Diard: Lijst der vogelen verzameld in Februarij en Maart 1829 te Buitenzorg
93. 1 pagina met 2 tekeningen van zeeslak. Bullaea alba v.Hass, Anjer
94. 1 pagina met tekening van inktvis. Loligo. L. meredionalis Nobis. Insula Amsterdam. Onychotheutis Bergii Lichtst
95. 1 pagina met tekening van inktvis. Octopus brevipes D'Orb
96. 1 pagina met 5 tekeningen van zeeslakken. Parmacella taeniata Nobis. In sylvis salaccae/Anjer/Dermatobranchus striatus v.Hass, Anjer/Derm. pustulatus v.H, Anjer
97. 1 pagina met tekening van ongewervelde. Anjer
98. 1 pagina met 5 tekeningen van ?. Solenosoma ovatum, Atlantische Zee/Solen. oblongum, Atlantic/Sol. comatum, Straat Sunda
99. 3 pagina's: 1) tekening slangenkop. Python bivittatus, Bengal, 2) tekening slangenlijf (cloaca). Java, 3) detailtekening slangenhuid. Python booa(?), Bengalen. Tab. 53
100. 1 pagina met (opgeplakte) tekening van hagedis. Draco viridis. Reinw(?)
Achterzijde stempel RMNH. In dit omslagvel zat ook een briefje met de volgende tekst: Deze tekeningen zijn kennelijk nog voor Kuhl gemaakt. Boie heeft hiervan in Indië kopieën gezien (brief aan Susanna, 26-30 juni 1826) en naar aanleiding daarvan een aantal wijzigingen in illustraties Erp. Java voorgesteld. O.a. wordt tekening Draco hematopogon van Harriang met name genoemd.
101. 
102. 1 pagina met 2 tekeningen van agaam. Java, Gedé
103. 1 pagina met 4 (opgeplakte) tekeningen van hagedis. Sc. multifasc, ad vivum, Java, Rein/Lac. tachydromus, Reinw Java
104. 1 pagina met 2 (opgeplakte) tekeningen van leguaan. Gal. guttar.(?) Java. 52. Ad vivum. Rwdt(?)
105. 1 pagina met 5 (opgeplakte) tekeningen van hagedissen. Java. H.?/Hemid. Frenatus
106. 1 pagina met (opgeplakte) tekening van varaan. Varan. bivittatus, Java
107. 1 pagina met (opgeplakte) tekening van varaan (kop). Varan. bivittatus. 19
108. 

Directory NNM001001488-1489 – dagboekfragment – Van Oort

1. Dagboekaantekeningen (afscheid Nederland 1825)

Directory NNM001001489-1495 – Van Oort 

1. Dagboekaantekeningen (Sumatra 1833)
2. Dagboekaantekeningen (Java)
3. Dagboekaantekeningen (over plaatselijke letterkunde op Java)
4. Dagboekaantekeningen (uittreksel dagboek voor G.M.C. van Sumatra; 2 kopiën)
5. Dagboekaantekeningen (rapport Sumatra vegetatie 1835)
6. Dagboekaantekeningen (rapport 1835 Sumatra)
7. Dagboekaantekeningen (Borneo) 

Directory NNM001001496-1499 – P. van Oort 

1. Dagboekaantekeningen (Sumatra)
2. Dagboekaantekeningen (Java)
3. Dagboekaantekeningen (zeereis van Timor naar Batavia)
4. Diverse dagboekaantekeningen
5. Verslag: Schluss zu der Reise auf Ceram und Toes(?) von dem Dr. Forsten auf Amboina 3/1 1843
-->















