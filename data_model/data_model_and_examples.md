## NCO Data model


### Object_types (metadata)

0. collection
1. Proceeding
2. Proceeding_page
3. Drawing
4. Drawing_page
5. Handwritten
6. Handwritten_page
7. Author
8. Place
9. Species
10. Keywords

for each `object_type`, the properties are listed below.


## Collection

Property | Sample Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:nco | 
Collection-owner |  | 
ncd:Collection |  | 
rdfs:label |  | 
ncd:collectionId |  | 
dc:title |  | 
dc:alternative |  | 
vcard:associatedPerson |  | 
dc:subject |  | 
dc:description |  |  
ncd:collectionExtent |   | 
ncd:collectionType |   | 
rdfs:label |  | 
ncd:conservationStatus |   | 
dc:format |   | 
dc:medium |   | 
ncd:expeditionNameCoverage |   | 
ncd:formationPeriod |   | 
Geospatial coverage |   | 
rdfs:label |   | 
ncd:physicalLocation |   | 
ncd:primaryGroupingPrinciple |   | 
rdfs:label |   | 
ncd:temporalCoverage |   | 
dc:accessRights |   | 

* This needs to be cleaned up
* I still need to add `Geospatial coverage` to `place`
* in the spreadsheet there is a `Sheet2` with metadata for the Proceedings, Drawings, and Handwritten **as** Proceedings, Drawings, and Handwritten. In other words, they sit on a level between collection and Proceeding. **Should these three be turned into independent object_types?**

## Proceeding

Proerty | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:nco:NNM001000864 | 
Location_in_archive |  RBR 9D-F: Verhandelingen | 
Naturalis_Label | NC_b_Oo_011_001 | 
Naturalis_ID | NNM001000864 | I stripped the `MMNAT01_AF_` prefix everywhere
Number_of_pages |   | 
Subtype | contents | 
Subsubtype | mixed | 
Description | Deel I van de Verhandelingen over de natuurlijke geschiedenis der Nederlandsche overzeesche bezittingen, door de Leden der Natuurkundige commissie in Indië en andere Schrijvers. Uitgegeven op last van den koning door C.J. Temminck (Leiden). 1839-1844.  Mammalia. Door P.v.Oort e.a. | 
Keywords |  | 
Period | 1839-1850 | 
Author | [some ID of object_type = `Author` here] | a reference to the oject_type = `Author`
Notes |  | 

* I'd rather have proper bibliographical details here
* Are the properties `Subtype` and `Subsubtype` useful? Andreas doubts it
* Is `Number_of_pages` useful?
* There is no property `Place` yet...


<!-- 
Lise has the following, which also applies to Drawings and Handwritten:

Object_type | Proceedings, Drawings, and Handwritten
----- | -----
ncd:Collection | ignore
ncd:collectionId | ignore
ncd:isPartOfCollection | ignore, is already in URN
dc:title | ignore
dc:alternative | 
vcard:associatedPerson | ignore
dc:description | ignore
ncd:collectionExtent | ignore
ncd:collectionType | ignore
rdfs:label | ignore
ncd:conservationStatus | ignore
dc:format | 
dc:medium | ignore
ncd:expeditionNameCoverage | ?
ncd:formationPeriod | ?
ncd:geoSpatialCoverage | put in `Place` field?
rdfs:label | ignore
ncd:physicalLocation | useful. Or use as `Owner` field?
ncd:primaryGroupingPrinciple | put in `Keywords` field (but how to extract again?)
rdfs:label | ignore
ncd:temporalCoverage | put in `Period` field
dc:accessRights | ignore

* Lise's tab Items is already covers by the data model and so can be ignored
* I fail to see the relevance of Lise's tab Pages 

-->

## Proceeding_page

Property | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:nco:NNM001000867.187 | 
Naturalis_ID | NNM001000867_187 | 
Scan_file | MMNAT01_AF_NNM001000867_187.jpg | 
Volume | 4 | 
Subtype | text | 
Page_number | 183 | 
Page_number_in_pencil |  | 
Keywords |  | 
Species_original | Salacia kalahiensis; Salacia exsculpta | 
Species | some Id of object_type = `Species` here | 
Drawing_caption |  | 
Drawing_reference | 
Illustrator | [some ID of object_type = `Author` here] | 
Place | [some ID of object_type = `Place` here] | 
Notes |  | 

* Is `Volume` useful?
* Is `Subtype` useful?
* Species_complete should only have ID of object_type = `Species`

## Drawing

Property | Value | Explanation
----- | ----- | -----
ID: [some db id] | generated automatically
URN | urn:cite:brill:nco.NNM001000001 | 
Location_in_archive | S2 I NC Anatomie 371B | 
Naturalis_label | NC_m_Oo_001_001 | 
Naturalis_ID | NNM001000001 | 
Number_of_pages |   | 
Subtype | Contents | 
Subsubtype | Drawing | 
Description | 2 pagina's: 1) 4 anatomische tekeningen. Podargus/Bucconis spec. Tapos, Jul.1827, 2) 16 anatomische tekeningen. Bucconis/Bucceros/Podargeus/Philedon/Paradisea spec. | 
B_W_drawing | Y | 
Keywords |   | 
Species |  | 
Period | 1824-1838 | 
Author | [some ID of object_type = `Author` here] | 
Notes |  | 

* Andreas is said the distinction between `Drawing` and `Drawing_page` is useless... I'm not sure...
* `Place` is still missing...

## Drawing_page

Property | Value | Explanation
----- | ----- | -----
ID: [some db id] | generated automatically
URN | urn:cite:brill:nco.NNM001000001 | 
Scan_file | MMNAT01_AF_NNM001000001_001.jpg | 
Description | 4 anatomische tekeningen. Podargus/Bucconis spec. Tapos, Jul.1827 | 

* Is this ok?

## Handwitten

Property | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:nco.NNM001001035 | 
Location_in_archive | C10: Mammalia, Aves, Amphibia | 
Naturalis_Label | NC_a_Ku_013_001
Naturalis_ID | NNM001001035 | 
Number_of_pages |  | 
Handwritten_type | Notes | 
Subtype | Cover | 
Subsubtype | Text | 
Description | Kartonnen omslag met bandjes; bevat twee bundels aantekeningen en een stapeltje losse blaadjes. Titel op zijkant: Kuhl Amphibien. Titel op voorkant: Amphibien Kuhl | 
Lakzegel |  | 
Keywords |  | 
Period | 1820-1823
Author | [some ID of object_type = `Author` here]
Addressee |  | 
Notes |  |

* `Place` is still missing...
* Is `Lakzegel` useful?

## Handwritten_page

Property | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:nco.NNM001001032.001 |  
Naturalis_ID | NNM001001032_001 | 
Scan_file_name | MMNAT01_AF_NNM001001032_001.jpg | 

* is this ok?

## Author

Property | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:indexTerm.Persons.00050001 | a generic Brill CITE URN
First_name | Heinrich | 
Initials | H. | 
Tussenvoegsel |  | 
Last_name | Kuhn | 
VIAf | http://viaf.org/viaf/57372096 | 

<!-- 
Lise has:

? | ?
----- | -----
skos:hiddenLabel | thse are the abbreviations that are the values of the `autafk` field in the `Auteurs` tab
rdf:Resource | VIAF ID (hurray!)
foaf:firstName | no initials
foaf:lastName | tussenvoegsel included here
foaf:name | is this relevant ??
rdfs:label | is rthis relevant ??
skos:altLabel | no values in Lise data
-->

## Place

Property | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically
URN | urn:cite:brill:indexTerm.Places.00000001 |
Geonames_Label |   | 
Geonames_URN |   | 

* This object_type is still unfinished, because I'm struggling to identify the places

## Species

Property | Value | Explanation
----- | ----- | -----
ID | [some db id] | generated automatically 
URN | ?????????????????? | 
Label |   | 
EOL URN	 |   | another URN
LSID |   | another URN

* This object_type is still unfinished, because I'm struggling to identify the places
* This spreadsheet has seveal other tabs with semantic data from Lise, that do not seem useful

<!-- 
Lise has:

? | ?
----- | -----
dwc:Taxon | ??
dwc:TaxonRank | ??
Ranks | sort of SKOS thing
boader | sort of SKOS thing
Ranks | sort of SKOS thing
narrower | sort of SKOS thing

* I fail to see the relevance of Lise's tab Anatomy
* I fail to see the relevance of Lise's tab Property or Attribute
* I need to look at Lise's data
-->

## Keywords

Object_type | Keywords
----- | -----
ID | [some db id]
URN | ??????????????????
?? | ???

* I conflated Taxonomy en Keywords
* Is there some Taxonomy or Thesaurus we can use? Like Library of Congres Subject Headings?

# Questions
dwc:Taxon kunnen we aan dwc:TaxonRank species toevoegen. of aan keywords? of allebei? 


Questions to Andreas:
* Take "MMNAT01_NNM001001035". Is er ook een MMNAT02 etc.? _No, leave out_
* Take "MMNAT01_NNM001001035". is er ook een NNM002 etc.? _No, but DON'T leave out_
* Leave out the distinction `cover` vs. `contents`

Questions to Lise
* wat is het onderscheid tussen collection en subcollection? (of: waarom onderscheid je drie collecties)
* waarom onderscheid je vijf subcollections en niet drie? volgens mij verwar je logisch (type) en fysiek (directory structure)
* bij items: waarom denk je dat Macklot de tekeningen van MMNAT01_NNM001000001 heeft gemaakt? Volgens de spreadsheet op de G Drive is het Schwaner. (En waarom gebruik je niet gewoon een afkorting die dan naar het tabblad auteurs verwijst)
* wat is de zin van het tabblad pages?
* boader = broader
* hoe verhoudt zich het tabblad anatomy tot de rest? In het spreadsheet op G zie ik één keer ergens "linkernier" staan (MMNAT01_NNM001000817), maar dat zie ik niet op dat tabblad
* idem voor property or attribute
* tabblad authors te prefereren boven tabblad authors in spreadsheet op G Drive? dc/foaf is beter, toch?

## 19 flavours and some translations

Number | Flavour | subflavour | subsubflavour
----- | ----- | ----- | ----
1 | reports (NL: verhandelingen, boeken) | contents (NL: binnenwerk) | mixed (NL: gemengd)
2 | reports | contents | text (NL: tekst)
3 | reports | contents | drawing (NL: tekening)
4 | drawings (NL: prenten, tekeningen) | contents | mixed
5 | drawings | contents | drawing
6 | drawings | contents | text
7 | drawings | cover (NL: omslag) | mixed
8 | drawings | cover | text
9 | handwritten | notes (NL: aantekeningen) | contents | mixed
10 | handwritten | notes | contents | text
11 | handwritten | notes | cover | mixed
12 | handwritten | notes | cover | text
13 | handwritten | correspondence (NL: correspondentie) | contents | mixed
14 | handwritten | correspondence | contents | text
15 | handwritten | correspondence | cover | text
16 | handwritten | diaries (NL: dagboeken) | contents | text
17 | handwritten | miscellaneous (NL: diversen) | contents | text
18 | handwritten | lists (NL: lijsten) | contents | text
19 | handwritten | lists | cover | text

* this seemed important at first, now less so
