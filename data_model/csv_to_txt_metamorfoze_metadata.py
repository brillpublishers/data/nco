import os
import csv
import uuid


DATA_FILE_PATH = os.path.join(os.path.dirname(__file__), 'data')
OBJECT_TYPES = [
    {
      'current_type': 'drawing_pages',
      'arkyves_type': 'image',
    },
    {
      'current_type': 'drawing',
      'arkyves_type': 'container',
    },
    {
      'current_type': 'handwritten_pages',
      'arkyves_type': 'handwritten',
    },
    {
      'current_type': 'handwritten',
      'arkyves_type': 'container',
    },
    {
      'current_type': 'proceeding_pages',
      'arkyves_type': 'book_illustration',
    },
    {
      'current_type': 'proceeding',
      'arkyves_type': 'book',
    },
    {
      'current_type': 'author',
      'arkyves_type': 'person',
    },
    {
      'current_type': 'collection',
      'arkyves_type': 'collection',
    },
    {
      'current_type': 'keywords',
      'arkyves_type': 'postpone',
    },
    {
      'current_type': 'place',
      'arkyves_type': 'postpone',
    },
]

def read_data(filename):
    input_file = os.path.join(DATA_FILE_PATH, 'metamorfoze_metadata', 'csv', f'{filename}.csv')
    output = []
    with open(input_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        header_row =  next(csv_reader)
        headers = [x.upper() for x in header_row]
        for row in csv_reader:
            if row[1]:
                row[0] = row[1].replace(':', '')
            else:
                row[0] = uuid.uuid4().hex
            output.append(list(zip(headers, row)))
    return output


def create_type(data, arkyves_type):
    new_data = []
    for row in data:
        row.append(('TYPE', arkyves_type))
        new_data.append(row)
    return new_data


def dump(filename, data, object_type):
    """
    adds data to text file from a list of list of tuples
    [[("ID", 1), ("URN, 1)], [("ID", 2), ("URN, 2)]]
    """
    output_file = os.path.join(DATA_FILE_PATH, 'metamorfoze_metadata', 'txt', f'{filename}.txt')
    with open(output_file, 'w') as txt_file:
        for row in data:
            for val in row:
                line = f'{val[0]} {val[1]}'
                txt_file.write(line)
                txt_file.write('\n')
            txt_file.write('$\n')


def main():
    for object_type in OBJECT_TYPES:
        current_type = object_type['current_type']
        arkyves_type = object_type['arkyves_type']
        if arkyves_type != 'postpone':
            filename = f'object_type_{current_type}'
            new_filename = f'object_type_{arkyves_type}'
            data = read_data(filename)
            data = create_type(data, arkyves_type)
            dump(new_filename, data, arkyves_type)


if __name__ == '__main__':
    main()
