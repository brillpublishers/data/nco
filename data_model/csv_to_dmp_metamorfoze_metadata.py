import os
import csv
import uuid


DATA_FILE_PATH = os.path.join(os.path.dirname(__file__), 'data')
OBJECT_TYPES = [
    # {
    #   'current_type': 'drawing_pages',
    #   'arkyves_type': 'image',
    #   'rows_to_skip': [],
    # },
    # {
    #   'current_type': 'drawing',
    #   'arkyves_type': 'container',
    #   'rows_to_skip': [],
    # },
    # {
    #   'current_type': 'handwritten_pages',
    #   'arkyves_type': 'handwritten',
    #   'rows_to_skip': [],
    # },
    # {
    #   'current_type': 'handwritten',
    #   'arkyves_type': 'container',
    #   'rows_to_skip': [],
    # },
    {
      'current_type': 'proceeding_pages',
      'arkyves_type': 'book_illustration',
      'rows_to_skip': [
          50, 2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098,
          2099, 2100, 2101, 2102, 2103, 2104, 2105, 2106, 2107, 2108, 2109, 2110],
    },
    {
      'current_type': 'proceeding',
      'arkyves_type': 'book',
      'rows_to_skip': [13]
    },
    # {
    #   'current_type': 'author',
    #   'arkyves_type': 'person',
    #   'rows_to_skip': [],
    # },
    # {
    #   'current_type': 'collection',
    #   'arkyves_type': 'collection',
    #   'rows_to_skip': [],
    # },
    # {
    #   'current_type': 'keywords',
    #   'arkyves_type': 'postpone',
    #   'rows_to_skip': [],
    # },
    # {
    #   'current_type': 'place',
    #   'arkyves_type': 'postpone',
    #   'rows_to_skip': [],
    # },
]

def read_data(filename, rows_to_skip, current_type):
    input_file = os.path.join(DATA_FILE_PATH, 'metamorfoze_metadata', 'csv', f'{filename}.csv')
    output = []
    with open(input_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', escapechar='\\', quotechar='"')
        header_row =  next(csv_reader)
        headers = [x.upper() for x in header_row]
        row_num = 2
        for row in csv_reader:
            if row_num not in rows_to_skip:
                if row[1]:
                    row[0] = row[1].replace(':', '')
                else:
                    row[0] = uuid.uuid4().hex
                if current_type == 'proceeding':
                    updated_data = update_proceeding_data(headers, row)
                    headers = updated_data['header_row']
                    row = updated_data['data_row']
                if current_type == 'proceeding_pages':
                    updated_data = update_proceeding_pages_data(headers, row)
                    headers = updated_data['header_row']
                    row = updated_data['data_row']
                output.append(list(zip(headers, row)))
            row_num += 1
    return output


def create_type(data, arkyves_type):
    new_data = []
    for row in data:
        row.append(('TYPE', arkyves_type))
        new_data.append(row)
    return new_data


def update_proceeding_data(header_row, data_row):
    id_num = data_row[0].split('.')
    data_row[0] = f'nco_{id_num[1]}'
    header_row.append('HIM')
    data_row.append('NCO')
    for idx, val in enumerate(header_row):
        if val == 'LOCATION_IN_ARCHIVE':
            header_row[idx] = 'ID_CAT'
        if val == 'NATURALIS_LABEL':
            header_row[idx] = 'ID_INV'
        if val == 'NATURALIS_LABEL':
            header_row[idx] = 'ID_INV'
        if val == 'NUMBER_OF_PAGES':
            header_row[idx] = 'BIBLIO_FORM'
        if val == 'DESCRIPTION':
            header_row[idx] = 'TITLE_SHOW'
        if val == 'KEYWORDS':
            header_row[idx] = 'KW'
            split_data_value = data_row[idx].split(':')
            data_row[idx] = f'nco_subject_{split_data_value[4].split(";")[0]}; nco_subject_{split_data_value[8]}'
        if val == 'PERIOD':
            header_row[idx] = 'DATE_ORIG'
        if val == 'AUTHOR':
            header_row[idx] = 'PERSON_AUTHOR'
            split_data_value = data_row[idx].split('.')
            data_row[idx] = f'nco_person_{split_data_value[2]}'
        if val == 'URN':
            del data_row[idx]
            del header_row[idx]
    for idx, val in enumerate(data_row):
        if not val:
            del data_row[idx]
            del header_row[idx]
    return {
      'header_row': header_row,
      'data_row': data_row
    }


def update_proceeding_pages_data(header_row, data_row):
    id_num = data_row[2].replace('_', '.')
    data_row[0] = f'nco_{id_num}'
    header_row.append('HIM')
    data_row.append('NCO')
    for idx, val in enumerate(header_row):
        if val == 'URN':
            del data_row[idx]
            del header_row[idx]
        if val == 'SCAN_FILE':
            header_row[idx] = 'URL_IMAGE'
        if val == 'VOLUME':
            del data_row[idx]
            del header_row[idx]
        # if val == 'KEYWORDS':
        #     header_row[idx] = 'KW'
        #     split_data_value = data_row[idx].split(':')
        #     data_row[idx] = f'nco_subject_{split_data_value[4]}'
    return {
      'header_row': header_row,
      'data_row': data_row
    }


def dump(filename, data, object_type):
    """
    adds data to text file from a list of list of tuples
    [[("ID", 1), ("URN, 1)], [("ID", 2), ("URN, 2)]]
    """
    output_file = os.path.join(DATA_FILE_PATH, 'metamorfoze_metadata', 'dmp', 'michael', f'{filename}.dmp')
    with open(output_file, 'w') as txt_file:
        for row in data:
            for val in row:
                line = f'{val[0]} {val[1]}'
                txt_file.write(line)
                txt_file.write('\n')
            txt_file.write('$\n')


def main():
    for object_type in OBJECT_TYPES:
        current_type = object_type['current_type']
        arkyves_type = object_type['arkyves_type']
        if arkyves_type != 'postpone':
            filename = f'object_type_{current_type}'
            new_filename = f'{arkyves_type}'
            data = read_data(filename, object_type['rows_to_skip'], object_type['current_type'])
            data = create_type(data, arkyves_type)
            dump(new_filename, data, arkyves_type)


if __name__ == '__main__':
    main()
