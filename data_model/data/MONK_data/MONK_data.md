## MONK data

## MONK data
* Really, the MONK data consists of two fields: ID and LABEL
* Each field takes a value. The ID is a set of co-ordinates on a scan. The LABEL is an ASCII string
* It would be easy to add additional fields, i.e. additional properties to the object_type = `MONK_data`. For example, annotations on the LABEL, such as strings in Unicode, the unabbreviated form of abbreviations, etc. etc.

In the context of NCO, MONK ingested the Metamorfoze images (see [here](https://drive.google.com/drive/folders/0BwzZnF8mahZJNldFOXZjZnMwNFU) ) but re-cut them. Unfortunately, we don't know the MONK file names (nor how to get to them from the Metamorfoze file names).

Below is sample of MONK's output. Note how:

* we only have the labels, i.e the bit between `<txt></txt>`. But not the annotations on the labels
* this is not a transcription, but an enumeration of labels per line per page
* labels preceded by `@` are "special codes" used to capture stuff that should be in UTF-8/Unicode (all labels are ASCII)
* Each string is a concatenation of parameters separated by hyphens. The eight one, _RECOG_, for example, means the label is identified by MONK itself. If it had read _HUMAN_, that would have meant the label was added manually. **There is no overview of all possible parameters, as far as I know**


#### Example of a a string split vertically:

```
['navis',
 'NNM001001033_0001',
 'line',
 '003',
 'y1=619',
 'y2=782',
 'zone',
 'RECOG',
 'x=1971',
 'y=0020',
 'w=0349',
 'h=0083',
 'ybas=0086',
 'nink=3698',
 'ns=6', 
'segm=COCOS8cocos']
```


#### Example of MONK output for NCO

```
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=0041-h=0432-ybas=0433-nink=4769-ns=1-segm=COCOS8cocos <txt>@VERT_CENTER_FOLD</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=0062-h=0432-ybas=0433-nink=6524-ns=2-segm=COCOS8cocos <txt>@VERT_CENTER_FOLD</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=0203-h=0432-ybas=0433-nink=6638-ns=3-segm=COCOS8cocos <txt>@VERT_CENTER_FOLD</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=1399-h=0432-ybas=0433-nink=6698-ns=4-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=1506-h=0432-ybas=0433-nink=6722-ns=5-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=1608-h=0432-ybas=0433-nink=14343-ns=8-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=1609-h=0432-ybas=0433-nink=14340-ns=7-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=1613-h=0432-ybas=0433-nink=14346-ns=9-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0102-y=0067-w=1769-h=0432-ybas=0433-nink=14328-ns=6-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=0032-h=0432-ybas=0433-nink=1755-ns=1-segm=COCOS8cocos <txt>9)</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=0173-h=0432-ybas=0433-nink=1869-ns=2-segm=COCOS8cocos <txt>@PLUS</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1369-h=0432-ybas=0433-nink=1929-ns=3-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1476-h=0432-ybas=0433-nink=1953-ns=4-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1578-h=0432-ybas=0433-nink=9574-ns=7-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1579-h=0432-ybas=0433-nink=9571-ns=6-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1583-h=0432-ybas=0433-nink=9577-ns=8-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1592-h=0432-ybas=0433-nink=9627-ns=9-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0132-y=0067-w=1739-h=0432-ybas=0433-nink=9559-ns=5-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0243-w=1541-h=0215-ybas=0433-nink=7876-ns=9-segm=COCOS8cocos <txt>levendige</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0243-w=1586-h=0215-ybas=0433-nink=7804-ns=4-segm=COCOS8cocos <txt>levendige</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0244-w=1439-h=0214-ybas=0433-nink=7872-ns=8-segm=COCOS8cocos <txt>Bombay</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0245-w=1430-h=0213-ybas=0433-nink=7822-ns=7-segm=COCOS8cocos <txt>brede</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0246-w=1425-h=0212-ybas=0433-nink=7819-ns=6-segm=COCOS8cocos <txt>Njei</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0246-w=1426-h=0212-ybas=0433-nink=7816-ns=5-segm=COCOS8cocos <txt>Celtis</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0279-w=1323-h=0174-ybas=0433-nink=198-ns=3-segm=COCOS8cocos <txt>stil</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0359-w=1216-h=0094-ybas=0433-nink=174-ns=2-segm=COCOS8cocos <txt>0,</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=0285-y=0430-w=0020-h=0023-ybas=0433-nink=114-ns=1-segm=COCOS8cocos <txt>x</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=1487-y=0243-w=0339-h=0215-ybas=0433-nink=7762-ns=8-segm=COCOS8cocos <txt>Dadap</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=1487-y=0243-w=0384-h=0215-ybas=0433-nink=7690-ns=3-segm=COCOS8cocos <txt>Dadap</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=1487-y=0243-w=0396-h=0215-ybas=0433-nink=8614-ns=9-segm=COCOS8cocos <txt>Mandor's</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=1487-y=0244-w=0237-h=0214-ybas=0433-nink=7758-ns=7-segm=COCOS8cocos <txt>@Tropinatus</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=1487-y=0245-w=0228-h=0213-ybas=0433-nink=7708-ns=6-segm=COCOS8cocos <txt>Pati</txt>
navis-NNM001001033_0001-line-001-y1=44-y2=552-zone-RECOG-x=1487-y=0246-w=0223-h=0212-ybas=0433-nink=7705-ns=5-segm=COCOS8cocos <txt>@Tropinatus</txt>
```

Compare the above with the below. This is MONK's output of the Prize Papers. Two differences:

* line numbers are missing
* images are identfied by their own file names, not by MONK file names
* Etienne says he wants the NCO export to be in PP format


#### Example of MONK output for the Prize Papers

```
0395_01_004_001-0003.jpg <x> 262 </x> <y> 145 </y> <w> 213 </w> <h> 92 </h> <txt>Vessel</txt>
0395_01_004_001-0003.jpg <x> 474 </x> <y> 152 </y> <w> 83 </w> <h> 76 </h> <txt>by</txt>
0395_01_004_001-0003.jpg <x> 556 </x> <y> 156 </y> <w> 167 </w> <h> 70 </h> <txt>which</txt>
0395_01_004_001-0003.jpg <x> 722 </x> <y> 147 </y> <w> 107 </w> <h> 80 </h> <txt>She</txt>
0395_01_004_001-0003.jpg <x> 828 </x> <y> 170 </y> <w> 101 </w> <h> 58 </h> <txt>was</txt>
0395_01_004_001-0003.jpg <x> 928 </x> <y> 150 </y> <w> 193 </w> <h> 76 </h> <txt>taken</txt>
0395_01_004_001-0003.jpg <x> 1120 </x> <y> 145 </y> <w> 155 </w> <h> 83 </h> <txt>That</txt>
0395_01_004_001-0003.jpg <x> 1274 </x> <y> 159 </y> <w> 101 </w> <h> 66 </h> <txt>her</txt>
0395_01_004_001-0003.jpg <x> 1374 </x> <y> 172 </y> <w> 171 </w> <h> 76 </h> <txt>Course</txt>
0395_01_004_001-0003.jpg <x> 1544 </x> <y> 177 </y> <w> 111 </w> <h> 45 </h> <txt>was</txt>
0395_01_004_001-0003.jpg <x> 1654 </x> <y> 171 </y> <w> 61 </w> <h> 50 </h> <txt>at</txt>
0395_01_004_001-0003.jpg <x> 1714 </x> <y> 157 </y> <w> 89 </w> <h> 64 </h> <txt>all</txt>
0395_01_004_001-0003.jpg <x> 1802 </x> <y> 145 </y> <w> 147 </w> <h> 80 </h> <txt>Times</txt>
0395_01_004_001-0003.jpg <x> 1948 </x> <y> 166 </y> <w> 133 </w> <h> 61 </h> <txt>when</txt>
0395_01_004_001-0003.jpg <x> 2080 </x> <y> 153 </y> <w> 119 </w> <h> 77 </h> <txt>the</txt>
0395_01_004_001-0003.jpg <x> 292 </x> <y> 239 </y> <w> 204 </w> <h> 81 </h> <txt>Weather</txt>
0395_01_004_001-0003.jpg <x> 508 </x> <y> 263 </y> <w> 150 </w> <h> 78 </h> <txt>would</txt>
0395_01_004_001-0003.jpg <x> 645 </x> <y> 264 </y> <w> 218 </w> <h> 77 </h> <txt>permits</txt>
0395_01_004_001-0003.jpg <x> 871 </x> <y> 256 </y> <w> 198 </w> <h> 55 </h> <txt>directed</txt>
0395_01_004_001-0003.jpg <x> 1076 </x> <y> 257 </y> <w> 193 </w> <h> 54 </h> <txt>towards</txt>
0395_01_004_001-0003.jpg <x> 1089 </x> <y> 257 </y> <w> 180 </w> <h> 52 </h> <txt>towards</txt>
0395_01_004_001-0003.jpg <x> 1277 </x> <y> 256 </y> <w> 237 </w> <h> 75 </h> <txt>Bordeaux</txt>
0395_01_004_001-0003.jpg <x> 1538 </x> <y> 268 </y> <w> 93 </w> <h> 40 </h> <txt>and</txt>
0395_01_004_001-0003.jpg <x> 1640 </x> <y> 247 </y> <w> 119 </w> <h> 60 </h> <txt>that</txt>
0395_01_004_001-0003.jpg <x> 1763 </x> <y> 259 </y> <w> 94 </w> <h> 49 </h> <txt>she</txt>
0395_01_004_001-0003.jpg <x> 1859 </x> <y> 254 </y> <w> 83 </w> <h> 55 </h> <txt>did</txt>
0395_01_004_001-0003.jpg <x> 1948 </x> <y> 262 </y> <w> 98 </w> <h> 50 </h> <txt>not</txt>
0395_01_004_001-0003.jpg <x> 2041 </x> <y> 255 </y> <w> 123 </w> <h> 54 </h> <txt>sail</txt>
0395_01_004_001-0003.jpg <x> 298 </x> <y> 349 </y> <w> 166 </w> <h> 87 </h> <txt>beyond</txt>
0395_01_004_001-0003.jpg <x> 473 </x> <y> 382 </y> <w> 53 </w> <h> 19 </h> <txt>or</txt>
0395_01_004_001-0003.jpg <x> 529 </x> <y> 356 </y> <w> 127 </w> <h> 47 </h> <txt>wide</txt>
0395_01_004_001-0003.jpg <x> 661 </x> <y> 358 </y> <w> 58 </w> <h> 77 </h> <txt>of</txt>
0395_01_004_001-0003.jpg <x> 724 </x> <y> 342 </y> <w> 81 </w> <h> 56 </h> <txt>the</txt>
0395_01_004_001-0003.jpg <x> 818 </x> <y> 351 </y> <w> 108 </w> <h> 48 </h> <txt>said</txt>
```

MONK is found [here](http://monkweb.nl/)

Log in with

UN: esuyver
PW: kefetadu

Etienne says: we need to export NCO out of MONK using the second style of output. I have no idea if Lambert will play along.

The alternative is to map MONK image file names to Metmorfoze file names. Lise already created a (limited) mapping. It is [here](MONK_Metamorfoze_mapping.md).

This is how Lise explains the mapping:

**Left** is Metamorfoze; **right** is MONK.

Example:

```MMNAT01_AF_NNM001001033_004.jpg": {"new_id": {"a": "NNM001001033_0005.jpg", "b": "NNM001001033_0006.jpg"}}```

This means: image `MMNAT01_AF_NNM001001033_004.jpg` from Metamorfoze is split into two images: 'a': `NNM001001033_0005.jpg` (left) and 'b': `NNM001001033_0006.jpg` (right). 





