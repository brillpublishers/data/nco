Lise is working on:

[Semantic Field Book Annotator](https://github.com/lisestork/SFB-Annotator)

[Natural History Collection Ontology](https://github.com/lisestork/NHC-Ontology)

[LIACS site](http://makingsense.liacs.nl/rdf/nhc-content/2018-04-04.html)

And [here](sfb_nc_sparql_endpoint.rdf) is a sample of the semantic data (in rdf).

## some explanation

The data are available online via the sparql endpoint: http://makingsense.liacs.nl/rdf4j-server/repositories/NC. You can also find them [here](sfb_nc_sparql_endpoint.rdf). This is in rdf/xml, but other formats are available, such as N-Triples, Turtle, N3, RDF/JSON, TriG, N-Quads, BinaryRDF, TriX, and JSON-LD). :smile:

## SFBA

MSI Functional requirement 7 says we need to include the Annotator (see also [here](https://gitlab.com/brillpublishers/making-sense/raw/master/wireframes/nco-wireframe-annotate.jpg) for an image)

It is also part of the [plan](https://gitlab.com/brillpublishers/making-sense/blob/master/NCO-plan.md) to include a SPARQL end point.

Furthermore, Lise and Etienne have the following plan:
* convert the Java of the Semantic Field Book Annotator (SFBA) into Python
* harmonize the backend and the frontend of the SFBA with the [Rescribe](https://gitlab.com/brillpublishers/rescribe) tool that Etienne has created
* install the SFBA on data.brill.com
* run the SFBA as an independen web app **and** as a tool within the NCO site (inline with the "widget" idea)
* use the semantic modelling of annotation as blueprint for other publications

Lise promised to draw a sketch of what the SFBA could look like in the NCO site and standalone, also in connection with the distinction between producing and consuming annotations (???).