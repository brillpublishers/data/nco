import os
import csv
import json


DATA_FILE_PATH = os.path.join(os.path.dirname(__file__), 'data', 'monk_NNM_20190625')
INPUT_DATA_FILE_PATH = os.path.join(DATA_FILE_PATH, 'input')
OUTPUT_DATA_FILE_PATH = os.path.join(DATA_FILE_PATH, 'output')


def get_files():
    all_files = []
    for file in os.listdir(INPUT_DATA_FILE_PATH):
        all_files.append(file)
    return all_files


def convert_data(filename):
    output = []
    input_file = os.path.join(INPUT_DATA_FILE_PATH, f'{filename}')
    with open(input_file, 'r', encoding='ISO-8859-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        for row in csv_reader:
            # print(row)
            if row[1] != '<x>' or row[3] != '</x>':
                print(f'<x></x> does not exist! for {row}')
            elif row[4] != '<y>' or row[6] != '</y>':
                print(f'<y></y> does not exist! for {row}')
            elif row[7] != '<w>' or row[9] != '</w>':
                print(f'<w></w> does not exist! for {row}')
            elif row[10] != '<h>' or row[12] != '</h>':
                print(f'<h></h> does not exist! for {row}')
            else:
                new_name = f'{row[0].split(".")[0]}.jpg'
                annotation = {
                    '@context': 'http://iiif.io/api/image/2/context.json',
                    '@id': f'https://iiif.arkyves.org/{new_name}',
                    'type': 'Annotation',
                    'body': {
                      'type': 'TextualBody',
                      'value': row[13],
                      'format': 'text/html'
                    },
                    'target': {
                      'id': f'https://iiif.arkyves.org/{new_name}/canvas/c0#xywh={row[2]},{row[5]},{row[8]},{row[11]}',
                      'type': 'Canvas'
                    }
                }
                output.append(annotation)
    return output


def dump(filename, data):
    output_file = os.path.join(OUTPUT_DATA_FILE_PATH, f'{filename}.json')
    with open(output_file, 'w') as f:
        json.dump(data, f)


def main():
    all_files = get_files()
    for filename in all_files:
        filename_array = filename.split('-')
        new_filename = f'{filename_array[0]}-{filename_array[1]}'
        data = convert_data(filename)
        dump(new_filename, data)


if __name__ == '__main__':
    main()
