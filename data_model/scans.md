# Scans (images)

## Overview
Size: ca. 17,000 scans (created by the _Metamorfoze_ project)

object type | number of scans
----- | -----
Proceedings ("Verhandelingen") | ca. 2000 scans
Drawings (including lithographs and sketches) | ca. 1550 scans
Handwritten (including (field)notes, diaries, lists, and correspondence) | ca. 13,500 scans

## Location
The material is digitized and stored in Naturalis, on the G drive, on a hard disk, and on a Brill server.

Storage | location
----- | -----
Naturalis | ?
G drive | https://drive.google.com/drive/folders/0Bx4AeK4b_jWcUFBIc3NxQ2dMS0E
hard disk | Etienne's desk
Brill server | ?

We use the scans **on the Brill server**.

## Directory Structure

The directory structure is as follows:

folder | object_type
----- | -----
MMNAT01_B1_F2_V3 | Reports
MMNAT01_B2_F2_V3 | Drawings
MMNAT01_B3_F2_V3 | Drawings
MMNAT01_B4_F2_V3 | Handwritten
MMNAT01_B5_F2_V3 | Handwritten

MMMNAT01 therefore identifies the collection, NCO, B1 the folder with a part of the collection, and F2_V3 is meaningless.

Each folder is divided into AF and PM folders, which contain JPEGs and TIFFs respectively. 

These folders may contain subfolders, with names like MMN001000864. These subfolders contain the scans of objects in the collection, such as a printed volume of the _Verhandelingen_. 

Each scan in these folders has a name like MMNAT01_AF_NNM001000864_001.jpg. The file name identifies the spread (being either one or two pages) within the object. In this example, 001 identfies the first page - in fact, the cover - of the volume of the _Verhandelingen_ identified by NNM001000864. The file name in its entirety therefore identifies folder (part of the collection), subfolder (image format), subfolder (object (book)), and page respectively.

MMNAT01_AF_NNM001000864_001.jpg | Example
----- | -----
MMNAT01 | folder (part of the collection)
AF | subfolder (image format)
NNM001000864 | subfolder (object (book))
001 | page
jpg | extension (image format)

It is not always so simple. MMNAT01_AF_NNM001000019_001.jpg, for example, sits in a folder called NNM001000001-NNM001000100 (itself in MMNAT01_B2_F2_V3), which is basically a collection of objects which don't have a folder of their own. So this is an object of the type "Drawing".

**We only use the .jpgs**
