## Data 

### Types of data 
1. Scans.  Created by Metamorfoze. We have .tiff and .jpg and only use .jpg. Scans represent objects of the type `drawing`, `proceeding_page`, and `handwritten_page` 
2. Metadata. Created by Metamorfoze. We have .json generated from .csv/.xlsx. Metadata pertains to objects of the type `collection`, `drawing_group`, `proceeding`, `proceeding_group` (??), `creator`, `place`, `species`, `subjects`  
3. MONK data (of unknown quantity and quality). Also known as "labels". In [MONK format](https://gitlab.com/brillpublishers/making-sense/blob/master/data_model/data/MONK_data/MONK_data.md), which is basically a label and a set of co-ordinates, in plain text.
4. Semantic data. However, at closer inspection, the semantic data runs out to be metadata, just more structured and rich. Currently, in .rdf but needs to be converted to .csv and then to json. So far, Lise has resisted all attempts to do this.

Note. There is a close connection between scans and (meta)data. However, there is also metadata at collection level. And it is possible to have scans without metadata. So the relation is not always 1:1.

### User annotations
* Once the NCO site is up, users will be generating data, using the Semantic Fieldbook Annotator (a tool). (Later, perhaps, other types of annotations and transcriptions will be added).
* This new data will be stored in a manner that is somehow compatible with the above. In addition, extra information is stored in a new sort odf semi-object (e.g., user ID, date stamp)

<!-- 
## More metadata
* what metadata do we have for the level above objects (collections) and for the level below (pages, etc.)
* if we allow the creation of semantic annotations on our site, we are in fact creating metadata
* such annotations can take the form of nano-publications (in which case they will need a DOI), which we could track
* such annotations rely on a form of citation: a bounding box (i.e coordinates in terms of pixels) connected to a page ID. 
* We need to create clarity about URNs, URIs, DOIs (if necessary?, object identifiers, file names, folder structures, etc.
* some (more) of the metadata is captured in rdf. Consider this a bunch of structure information concerning an object
* in addition, we have rdf for animal names identified on the pages (i.e parts of parts of objects)
* the intention is to create rdf for illustrations, too
* can we use the rdf internally in the site? or is it only for the SPARQL endpoint? Cf. http://dutchshipsandsailors.nl/data/home

### Example
MMNAT01_AF_NNM001000864 might make a good example for our development. It can be found in MMNAT01_B4_F2_V3. It is an object of the type "field notes".
-->

### URNs
* We are going to use CITE URNs to identify, connect, and retrieve data
* The syntax of a CITE URN is `urn:cite:CITENAMESPACE:COLLECTION.OBJECTID@subreference`
* The CITE ontology allows for `collections` and `objects`
* (Because we have collection level metadata, in the NCO ontology, the NCO collection is itself an object.)

<!--
* CITE objects are not the same thing as database objects. For example, a "collection" is not a CITE object, but the collection metadata _are_ a database object. 
* Database objects are "physical", they are files. CITE objects are "logical".
* The same logical CITE object - say, a drawing of a butterfly - may have multiple physical manifestations, among them database objects like a .jpg file and metadata in a .json file
-->

* Logically, objects can have a hierarchical relation. For example, objects of the type `Proceeding_page` are children of objects of the type `Proceeding`
* This relation is expressed in the CITE URN, e.g. `urn:cite:CITENAMESPACE:COLLECTION.OBJECTID.OBJECTPARTID@subreference`
* Objects have properties. A .jpg, although a database objetc in some sense, is not an object in the NCO ontology. Instead, the object represents, e.g., a drawing, and the file name of the .jpg is a value of the property `iiif-url` (or something like that). Likewise, the MONK labels are values of the property `has_text` (or something like that).
* Note that is is possible to refer to a .jps with a CITE URN, using pixels or co-ordinates as subreference.
* To resolve the URNs, add a base URL, e.g. `https://dh.brill.com/pso/nco`

### URNs examples

object | URN | remark
----- | ----- | -----
scan | urn:cite:brill:nco.NNM001000019.001 |
page_place | urn:cite:brill:nco.NNM001000019.001.AF@x="51.2867602",y="51.6918741" | not sure we will user this
MONK data | ? |
collection | urn:cite:brill:nco | 
proceeding | urn:cite:brill:nco.NNM001000864 | 
proceeding_page | urn:cite:brill:nco.NNM001000864.001 | 
drawing | urn:cite:brill:nco.NNM001000019.001 | 
handwritten | urn:cite:brill:nco.NNM001001032.001 | 
handwritten_page | urn:cite:brill:nco.NNM001001032.001 | 
author | VIAF ID ?? | 
place | geonames ID ?? | 
species | ?? | 
keywords | ?? | if all else fails: Brill CITE URN

### Another example
Files `MMNAT01_AF_NNM001000001_001.jpg` and `MMNAT01_AF_NNM001000001_002.jpg` sit in folder: NNM001000001-NNM001000100 (in MMNAT01_B2_F2_V3). These files represent objects of the type `drawing`. Although there is no folder with that name, these objects belong to an object of the type `drawing_group`, with idenitfier `NNM001000001`. We therefore get the following URNs:

reference | urn
----- | -----
object of type `drawing` | `urn:cite:brill:nco.NNM001000001.001`
object of type `drawing_group` | `urn:cite:brill:nco.NNM001000001`
subreference to an area on an object of type `drawing` | `urn:cite:brill:nco.NNM001000001.001.AF@x="51.2867602",y="51.6918741"`

* These areas are knonw as "bounding boxes".
* The actual file name, or path, is a value of a property of the object with URN `urn:cite:brill:nco.NNM001000001.001`
* Note how the URN expresses a hierarchy
* The string `MMNAT01` is superfluous and therefore omitted
* `nco` is a publication acronym, like AMO, and used for access management
