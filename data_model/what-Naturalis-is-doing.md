## Overview 
* Eulalia and Silvia are adding and cleaning data
* to the objects of the type **drawing_pages** they are adding places and species and dates
* to the objects of the type **handwritten_pages** they are adding places and species and dates. The idea is to start with subtype "Notes".

## My email from May 16th, 2019

Hi all,

@Eulalia, bijgevoegd de spreadsheets waarin je de additionele informatie kunt invullen. 

Het ene is een lijst van de drawing_pages, en dat zijn dus pagina’s met tekeningen (waarbij elke regel in de spreadsheet correspondeert met één scan van zo’n pagina met tekeningen). Ik heb kolommen E t/m K toegevoegd, en daar kun je het volgende invullen: 

Place_verbatim	Place_interpreted	Place_current	Species_verbatim	Species_interpreted	Species_current	Date

Het andere is een lijst van handwritten_pages. Dat is de spreadsheet voor Sylvia. Hier correspondeert elke regel met een scan van een pagina uit het handgeschreven materiaal. Ik kon helaas niet op type filteren, dat kan alleen in het object “handwritten” , dus één niveau hoger. Maar hier is een lijst van object_type = handwritten die van het type “notes “zijn:

1032-1037
1039-1045
1055-1062
1071-1073
1080-1083
1087
1096-1141
1144-1178
1264
1386-1390
1415

Ik hoop dat ik dit juist gekopieerd heb….

Ik heb kolommen E t/m K toegevoegd, zodat je daar de volgende informatie kunt toevoegen.

Place_verbatim	Place_interpreted	Place_current	Species_verbatim	Species_interpreted	Species_current	Date

Ik geloof dat je niet van plan was Sylvia ook de species te laten invullen. In dat geval kun je kolom H t/m J met een gerust hart verwijderen. 

Tot slot wou ik nog even zeggen dat ik het ongelofelijk goed vind dat Sylvia en jij dit willen doen.

Groet, Ernest

## The path of the empty files
G:\Projects\Making Sense\Making Sense Interface\data 
