# NCO

## Natuurkundige Commissie Online
nco = Natuurkundige Commissie Online. This is a primary sources collection, consisting of digitized printed and handwritten material and drawings.

The source files are scans (jpegs, but we also have tiffs), metadata (excel, converted to dmp), and MONK labels (likewise converted to dmp).

## Making Sense

The application is developed in the context of the [Making Sense](https://brill.com/page/digitalhumanities/making-sense-of-illustrated-handwritten-archives) project.

## Data and documentation

This repo contains the current data and documentation. We have removed the previous web interface from Eldarion and some extra files to make it easier to browse.
See commit https://gitlab.com/brillpublishers/data/nco/commit/d69dbf04a094803a4bf19e59620bff7ef657a37a to restore history if needs be.

We are using the Kosmeo React frontend in a [branch of the Kosmeo front-end](https://gitlab.com/brillpublishers/arkyves-frontend/tree/making-sense)

## Converting the Excel Data files to .dmp files

We are using the [Textbase](https://gitlab.com/brillpublishers/code/textbase) Docker image to convert the Excel files in the ./data/ directory to .dmp files that can be loaded into Kosmeo.

The command to use is:

`docker run --rm -ti -v $(pwd):/data registry.gitlab.com/brillpublishers/code/textbase:latest`

This operates on files in your current directory, and assumes that you are logged in to the gitlab docker registry or have built that Docker image locally.
